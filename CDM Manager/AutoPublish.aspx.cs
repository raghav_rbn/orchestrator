﻿using CDMManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CDM_Manager
{
    public partial class AutoPublish : System.Web.UI.Page
    {
        private string CDManagerDB = ConfigurationManager.ConnectionStrings["CDM_DevConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            string ResponseToken;

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers["Content-Type"] = Constants.AuthContentType;
                    String authString = "{\"email\": \"" + Request["login"] + "\", \"password\": \"" + Request["password"] + "\"}";
                    byte[] response = client.UploadData(Constants.AuthUrl, Constants.AuthMethod, Encoding.ASCII.GetBytes(authString));

                    ResponseToken = System.Text.Encoding.UTF8.GetString(response);
                    if (ResponseToken.Contains("\"id\":"))
                    {
                        var serializer = new JavaScriptSerializer();
                        LoginToken a = serializer.Deserialize<LoginToken>(ResponseToken);
                        Session.Add("ResponseToken", a.token);
                        Session.Add("UserName", Request["login"]);
                        RefreshCells(a.token);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Failed to authenticate", "alert('Failed to authenticate')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "Page_Load");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Failed to authenticate", "alert('Failed to authenticate')", true);
            }
        }

        private void RefreshCells(string loginToken)
        {
            try
            {
                DataTable dataTable = new DataTable("CellDefinition");
                Dictionary<int, string> cellTypeDictionary = Utilities.LoadDictionaryFromTable("CDCellTypeID", "CellTypeDesc", "[Cdef].[CellType]");
                Dictionary<int, string> cellColorDictionary = Utilities.LoadDictionaryFromTable("CDColorID", "CellColor", "[Cdef].[CellColor]");
                Dictionary<int, string> cellInputDictionary = Utilities.LoadDictionaryFromTable("CDInputID", "InputDesc", "[Cdef].[CellInput]");
                Dictionary<int, string> cellLayoutDictionary = Utilities.LoadDictionaryFromTable("CDLayoutID", "LayoutDesc", "[Cdef].[CellLayout]");

                using (SqlConnection sql = new SqlConnection(CDManagerDB))
                {
                    string sqlGetDataSource = "select Id, Title, CDInputID, CDLayoutID, CDCellTypeId, XRefId, ClusterCellId, CDColorID, Tags, IsModal " +
                        "from Cdef.CellDefinition " +
                        "where CellStatus = 1 " +
                        "and CDInputID = 1 " +
                        "and ClusterCellId is not null ";

                    SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                    SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                    adapter.Fill(dataTable);

                    if (dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in dataTable.Rows)
                        {
                            try
                            {
                                string ClusterCellId = row["ClusterCellId"].ToString();
                                if (String.IsNullOrEmpty(ClusterCellId)) ClusterCellId = "-1";
                                if (cellInputDictionary[int.Parse(row["CDInputID"].ToString())].Equals("NSD"))  //NSD Input 
                                {
                                    DataTable sourceDataTable = new DataTable("CellDefinition");

                                    string sqlData = "select CellSource, CellDataType, CellLocation, Period1, Period2, Period3, Heading1, Heading2, Heading3, XaxisLabel, YaxisLabel, LineColor, NumberOfEvents, Legend " +
                                                    "from Cdef.CellSource " +
                                                    "where CellDefinitionId =  " + row["Id"];

                                    selectDataSource = new SqlCommand(sqlData, sql);
                                    adapter = new SqlDataAdapter(selectDataSource);
                                    adapter.Fill(sourceDataTable);
                                    string cellTypeDesc = cellTypeDictionary[int.Parse(row["CDCellTypeId"].ToString())];
                                    string cellColor = null;
                                    if (!String.IsNullOrEmpty(row["CDColorID"].ToString()))
                                        cellColor = cellColorDictionary[int.Parse(row["CDColorID"].ToString())];
                                    string cellLayout = cellLayoutDictionary[int.Parse(row["CDLayoutID"].ToString())];

                                    if (sourceDataTable.Rows.Count > 0)
                                    {
                                        DataRow sourceRow = sourceDataTable.Rows[0];
                                        if (cellLayout.Equals("Single")) //Layout = Single
                                        {
                                            Utilities.RefreshNSDSingleCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), sourceRow["CellSource"].ToString(), sourceRow["CellDataType"].ToString(), sourceRow["CellLocation"].ToString(), cellTypeDesc, cellColor, row["Tags"].ToString());
                                        }
                                        else if (cellLayout.Equals("Wide") && !cellTypeDesc.Equals("GraphView")) //Layout = Wide and CellType = Non GraphView
                                        {
                                            Utilities.RefreshNSDWideCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), cellLayout, sourceRow["CellSource"].ToString(), sourceRow["CellDataType"].ToString(), sourceRow["CellLocation"].ToString(), cellTypeDesc, cellColor, row["Tags"].ToString(), sourceRow["Period1"].ToString(), sourceRow["Period2"].ToString(), sourceRow["Period3"].ToString(), sourceRow["Heading2"].ToString(), sourceRow["Heading3"].ToString());
                                        }
                                        else if (cellLayout.Equals("Wide") && cellTypeDesc.Equals("GraphView")) //Layout = Wide and CellType = GraphView
                                        {
                                            Utilities.RefreshWideGraphCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), cellLayout, sourceRow["CellSource"].ToString(), sourceRow["CellDataType"].ToString(), sourceRow["CellLocation"].ToString(), cellTypeDesc, cellColor, row["Tags"].ToString(), sourceRow["NumberOfEvents"].ToString(), sourceRow["LineColor"].ToString(), sourceRow["Legend"].ToString(),sourceRow["Heading3"].ToString());
                                        }
                                        else if ((cellLayout.Equals("Tall")) && !cellTypeDesc.Equals("GraphView")) //Layout = Wide and CellType = Non GraphView//Layout = Tall
                                        {
                                            string[] CellSource = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellSource")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] CellDataType = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellDataType")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] CellLocation = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellLocation")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] xAxisLabel = sourceDataTable.AsEnumerable().Select(r => r.Field<string>("XaxisLabel")).ToArray();
                                            Utilities.RefreshNSDTallCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), cellLayout, CellSource, CellDataType, CellLocation, cellTypeDesc, cellColor, row["Tags"].ToString(), sourceRow["Heading1"].ToString(), sourceRow["Period1"].ToString(), xAxisLabel);
                                        }
                                        else if ((cellLayout.Equals("Tall")) && cellTypeDesc.Equals("GraphView")) //Layout = Wide and CellType = Non GraphView//Layout = Tall
                                        {
                                            Utilities.RefreshTallGraphCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), cellLayout, sourceRow["CellSource"].ToString(), sourceRow["CellDataType"].ToString(), sourceRow["CellLocation"].ToString(), cellTypeDesc, cellColor, row["Tags"].ToString(), sourceRow["NumberOfEvents"].ToString(), sourceRow["LineColor"].ToString(), sourceRow["Legend"].ToString(), sourceRow["YaxisLabel"].ToString(), sourceRow["Heading1"].ToString());
                                            //public static int RefreshTallGraphCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string CellSource, string CellDataType, string CellLocation, string CellType, string CellColor, string Tags, string NumOfEvents, string CDLineColor, string Legend, string YAxis, string Heading)
                                        }
                                        else if (cellLayout.Equals("Quad") && cellTypeDesc.Equals("GraphView")) //Layout = Quad and CellType = GraphView
                                        {
                                            string[] CellSource = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellSource")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] CellDataType = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellDataType")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] CellLocation = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellLocation")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] xAxisLabel = sourceDataTable.AsEnumerable().Select(r => r.Field<string>("XaxisLabel")).ToArray();
                                            string[] lineColor = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("LineColor")).ToArray().Select(x => x.ToString()).ToArray();
                                            for (int k = 0; k < lineColor.Length; k++)
                                            {
                                                //ModalLineColor[k] = cellColorDictionary[int.Parse(row["CDColorID"].ToString())];
                                                lineColor[k] = cellColorDictionary[int.Parse(lineColor[k])];
                                            }
                                            Utilities.RefreshNSDQuadGraphViewCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), cellLayout, CellSource, CellDataType, CellLocation, cellTypeDesc, cellColor, row["Tags"].ToString(), xAxisLabel, lineColor, sourceRow["Period1"].ToString(), sourceRow["NumberOfEvents"].ToString());
                                        }
                                        else if (cellLayout.Equals("Quad") && !cellTypeDesc.Equals("GraphView")) //Layout = Quad and CellType = Non GraphView
                                        {
                                            string[] CellSource = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellSource")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] CellDataType = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellDataType")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] CellLocation = sourceDataTable.AsEnumerable().Select(r => r.Field<int>("CellLocation")).ToArray().Select(x => x.ToString()).ToArray();
                                            string[] xAxisLabel = sourceDataTable.AsEnumerable().Select(r => r.Field<string>("XaxisLabel")).ToArray();
                                            string[] heading = { sourceRow["Heading1"].ToString(), sourceRow["Heading2"].ToString(), sourceRow["Heading3"].ToString() };  //sourceDataTable.AsEnumerable().Select(r => r.Field<string>("Heading1")).ToArray();
                                            string[] period = { sourceRow["Period1"].ToString(), sourceRow["Period2"].ToString() , sourceRow["Period3"].ToString() };//sourceDataTable.AsEnumerable().Select(r => r.Field<string>("Period1")).ToArray();
                                            Utilities.RefreshNSDQuadNonGraphViewCellDefinition(loginToken, Int32.Parse(row["Id"].ToString()), Int32.Parse(ClusterCellId), row["Title"].ToString(), cellLayout, CellSource, CellDataType, CellLocation, cellTypeDesc, cellColor, row["Tags"].ToString(), heading, period, xAxisLabel);
                                        }
                                        ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " refreshed successfully ", "RefreshCells");
                                    }
                                    else
                                        ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " does not have cellsource/cell data type/cell location stored " + DateTime.Now.ToString("HH:mm:ss tt"), "RefreshCells");
                                }
                                else if (cellInputDictionary[int.Parse(row["CDInputID"].ToString())].Equals("Other"))  //Other Input
                                {
                                    //int responseObjectid = Utilities.RefreshNSDSingleCellDefinition(loginToken, row["Id"], row["Title"], string CellSource, string CellDataType, string CellLocation, row["CDCellTypeId"], row["CDColorID"], row["Tags"]);
                                }

                                if (Boolean.Parse(row["IsModal"].ToString())) //This is Modal cell type
                                {
                                    DataTable modalDataTable = new DataTable("ModalContent");
                                    string sqlModalData = "select ModalContentType, ModalLongTitle, ModalImageNewsPath " +
                                                    "from Cdef.ModalContent " +
                                                    "where CellDefinitionId =  " + row["Id"];

                                    SqlCommand selectModalDataSource = new SqlCommand(sqlModalData, sql);
                                    adapter = new SqlDataAdapter(selectModalDataSource);
                                    adapter.Fill(modalDataTable);
       
                                    if (modalDataTable.Rows.Count > 0)
                                    {
                                        int updatedModalCellId = -1;
                                        DataRow modalDetailRow = modalDataTable.Rows[0];
                                        string ModalLayout = modalDetailRow["ModalContentType"].ToString();

                                        if (ModalLayout.Equals("webView"))
                                        {
                                            updatedModalCellId = Utilities.ModalCellPost(loginToken, Int32.Parse(row["ClusterCellId"].ToString()), modalDetailRow["ModalContentType"].ToString(), modalDetailRow["ModalLongTitle"].ToString(), modalDetailRow["ModalImageNewsPath"].ToString(), null, null, null, null, null, null, null);
                                        } 
                                        else if (ModalLayout.Equals("Graph"))
                                        {
                                            string[] ModalCellSource = { };
                                            string[] ModalDataType = { };
                                            string[] ModalLocation = { };
                                            string[] ModalPeriod = { };
                                            string[] ModalNumOfEvents = { };
                                            string[] ModalLineColor = { };
                                            string[] ModalLabel = { };

                                            DataTable modalSourceDataTable = new DataTable("ModalSource");

                                            string modalSqlData = "select ModalSourceCounter, ModalCellSource, ModalCellDataType, ModalCellLocation, ModalNumOfEvents, ModalPeriod, ModalLineColor, ModalLabel " +
                                                            "from Cdef.ModalSources " +
                                                            "where CellDefinitionId =  " + row["Id"];

                                            SqlDataAdapter ModalAdapter = new SqlDataAdapter(new SqlCommand(modalSqlData, sql));
                                            ModalAdapter.Fill(modalSourceDataTable);

                                            if (modalSourceDataTable.Rows.Count > 0)
                                            {
                                                ModalCellSource = modalSourceDataTable.AsEnumerable().Select(r => r.Field<int>("ModalCellSource")).ToArray().Select(x => x.ToString()).ToArray();
                                                ModalDataType = modalSourceDataTable.AsEnumerable().Select(r => r.Field<int>("ModalCellDataType")).ToArray().Select(x => x.ToString()).ToArray();
                                                ModalLocation = modalSourceDataTable.AsEnumerable().Select(r => r.Field<int>("ModalCellLocation")).ToArray().Select(x => x.ToString()).ToArray();
                                                ModalNumOfEvents = modalSourceDataTable.AsEnumerable().Select(r => r.Field<int>("ModalNumOfEvents")).ToArray().Select(x => x.ToString()).ToArray();
                                                ModalLabel = modalSourceDataTable.AsEnumerable().Select(r => r.Field<string>("ModalLabel")).ToArray();
                                                ModalPeriod = modalSourceDataTable.AsEnumerable().Select(r => r.Field<string>("ModalPeriod")).ToArray();
                                                ModalLineColor = modalSourceDataTable.AsEnumerable().Select(r => r.Field<int>("ModalLineColor")).ToArray().Select(x => x.ToString()).ToArray();

                                                for (int k=0; k< ModalLineColor.Length; k++)
                                                {
                                                    //ModalLineColor[k] = cellColorDictionary[int.Parse(row["CDColorID"].ToString())];
                                                    ModalLineColor[k] = cellColorDictionary[int.Parse(ModalLineColor[k])];
                                                }
                                                updatedModalCellId = Utilities.ModalCellPost(loginToken, Int32.Parse(row["ClusterCellId"].ToString()), modalDetailRow["ModalContentType"].ToString(), modalDetailRow["ModalLongTitle"].ToString(), modalDetailRow["ModalImageNewsPath"].ToString(), ModalCellSource, ModalDataType, ModalLocation, ModalPeriod, ModalNumOfEvents, ModalLineColor, ModalLabel);
                                            }
                                            else
                                            {
                                                ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " Is a Modal cell. It does not have data in ModalSources table ", "RefreshCells");
                                            }
                                        }
                                     
                                        if (updatedModalCellId>0)
                                            ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " modal PUT succeeded ", "RefreshCells");
                                        else
                                            ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " modal PUT failed ", "RefreshCells");
                                    }
                                    else
                                    {
                                        ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " does not have modal details ", "RefreshCells");
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                ExceptionUtility.LogException(e, "RefreshCells");
                                ExceptionUtility.LogMessage("Cell definition id: " + row["Id"] + " not refreshed. Exception message " + e.Message, "RefreshCells");
                                continue;
                            }
                        }
                    }
                    sql.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshCells");
            }
        }

        private DataTable GetCellListToRefresh()
        {
            DataTable dataTable = new DataTable();

            try
            {
                using (SqlConnection sql = new SqlConnection(CDManagerDB))
                {
                    string sqlGetDataSource = "select Id, CDInputID, CDLayoutID, CDCellTypeId, XRefId, ClusterCellId " +
                        "from Cdef.CellDefinition " +
                        "where CellStatus = 1 " +
                        "and (convert(varchar(10), ExpirationDate, 102)>=convert(varchar(10), getdate(), 102) " +
                        "or ExpirationDate is null)";
                    SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                    SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                    adapter.Fill(dataTable);
                    sql.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "GetCellList");
            }
            return dataTable;
        }
    }
}