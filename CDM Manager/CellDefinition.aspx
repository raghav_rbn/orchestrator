﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CellDefinition.aspx.cs" Inherits="CDMManager.CellDefinition" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cell Definition</title>
    <link href="CDMStyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style7 {
            margin-left: 179px;
        }
        .auto-style8 {
            margin-left: 183px;
            margin-top: 2px;
        }
        .auto-style10 {
            margin-left: 183px;
        }
        .auto-style11 {
            margin-left: 15px;
        }
    </style>
</head>
<body>
    
    <form id="CellDefinitionForm" runat="server">
    <div>
    
    </div>
        <br />
        <br />
        <asp:Label ID="CellIdLabel" runat="server" Text="Cell Id"></asp:Label>
        <asp:TextBox ID="CellId" runat="server" ReadOnly="True" style="margin-left: 24px" BackColor="Silver"></asp:TextBox>
        <br />
        <br />
&nbsp;<asp:Label ID="StatusLabel" runat="server" Text="Status"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Status" runat="server" OnSelectedIndexChanged="Status_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="1">Active</asp:ListItem>
            <asp:ListItem Value="0">InActive</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        Ad &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Ad" runat="server">
            <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
            <asp:ListItem Value="1">Yes</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        Cell Title&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="Title" runat="server" MaxLength="16"></asp:TextBox>
        <asp:RequiredFieldValidator ID="CellTitleValidator" runat="server" ControlToValidate="Title" ErrorMessage="Cell Title is required"></asp:RequiredFieldValidator>
        <br />
        <br />
        Long Title &nbsp;<asp:TextBox ID="LongTitle" runat="server" MaxLength="32"></asp:TextBox>
        <br />
        <br />
        Description&nbsp;
        <asp:TextBox ID="CellDescription" runat="server" MaxLength="256"></asp:TextBox>
        <br />
        <br />
        Input
        <asp:DropDownList ID="CDInput" runat="server" DataSourceID="CDMDev" AppendDataBoundItems="True" DataMember="DefaultView" OnSelectedIndexChanged="CDInput_SelectedIndexChanged" AutoPostBack="True" CausesValidation="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="CDMDev" runat="server" ConnectionString="<%$ ConnectionStrings:CDM_DevConnectionString %>" SelectCommand="SELECT [CDInputID], [InputDesc] FROM [Cdef].[CellInput]"></asp:SqlDataSource>
        <br />
        <br />
        Layout
        <asp:DropDownList ID="CDLayout" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev" AutoPostBack="True" OnSelectedIndexChanged="CDLayout_SelectedIndexChanged">
        </asp:DropDownList>
        <br />
        <br />
        Cell Type<asp:DropDownList ID="CDCellType" runat="server" OnSelectedIndexChanged="CDCellType_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True">
        </asp:DropDownList>
        <asp:Panel ID="PanelNSDSingle" runat="server" Height="25px" style="margin-left: 176px" Width="754px">
            <asp:Label ID="LabelCellSource" runat="server" Text="CellSource"></asp:Label>
            <asp:DropDownList ID="DropDownListCellSource" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="CDMDev" OnSelectedIndexChanged="DropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelCellDataTye" runat="server" Text="Cell Data Type"></asp:Label>
            <asp:DropDownList ID="DropDownListDataType" runat="server" AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelLocation" runat="server" Text="Location"></asp:Label>
            <asp:DropDownList ID="DropDownListLocation" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListLocation_SelectedIndexChanged">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="PanelNSDTall" runat="server" CssClass="auto-style7" Height="107px" Width="1100px">
            <asp:Label ID="LabelNSDTallCellSource1" runat="server" Text="CellSource1"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallSource1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellDataType1" runat="server" Text="Cell Data Type1"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallType1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellLocation1" runat="server" Text="Location 1"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallLocation1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListLocation_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallAxisLabel1" runat="server" Text="X-Axis Label1"></asp:Label>
            <asp:TextBox ID="TextBoxXAxisLabel1" runat="server" MaxLength="32"></asp:TextBox>
            <br />
            <asp:Label ID="LabelNSDTallCellSource2" runat="server" Text="CellSource2"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallSource2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellDataType2" runat="server" Text="Cell Data Type2"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallType2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellLocation2" runat="server" Text="Location 2"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallLocation2" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallAxisLabel2" runat="server" Text="X-Axis Label2"></asp:Label>
            <asp:TextBox ID="TextBoxXAxisLabel2" runat="server" MaxLength="32"></asp:TextBox>
            <br />
            <asp:Label ID="LabelNSDTallCellSource3" runat="server" Text="CellSource3"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallSource3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellDataType3" runat="server" Text="Cell Data Type3"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallType3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellLocation3" runat="server" Text="Location 3"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallLocation3" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallAxisLabel3" runat="server" Text="X-Axis Label3"></asp:Label>
            <asp:TextBox ID="TextBoxXAxisLabel3" runat="server" MaxLength="32"></asp:TextBox>
            <br />
            <asp:Label ID="LabelNSDTallCellSource4" runat="server" Text="CellSource4"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallSource4" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellDataType4" runat="server" Text="Cell Data Type4"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallType4" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallCellLocation4" runat="server" Text="Location 4"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDTallLocation4" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDTallAxisLabel4" runat="server" Text="X-Axis Label4"></asp:Label>
            <asp:TextBox ID="TextBoxXAxisLabel4" runat="server" MaxLength="32"></asp:TextBox>
            <br />
            &nbsp;&nbsp;&nbsp;
            </asp:Panel>
        <asp:Panel ID="PanelNSDTallExclusive" runat="server" CssClass="auto-style8" Height="29px">
            <asp:Label ID="LabelNSDTallHeading" runat="server" Text="Heading"></asp:Label>
&nbsp;
            <asp:TextBox ID="NSDTallHeading" runat="server" MaxLength="32" Width="67px"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="LabelNSDTallPeriod" runat="server" Text="Period "></asp:Label>
            &nbsp;
            <asp:DropDownList ID="DropDownListNSDTallPeriod" runat="server">
                <asp:ListItem>Current</asp:ListItem>
                <asp:ListItem>1 Day</asp:ListItem>
                <asp:ListItem>2 Days</asp:ListItem>
                <asp:ListItem>3 Days</asp:ListItem>
                <asp:ListItem>4 Days</asp:ListItem>
                <asp:ListItem>5 Days</asp:ListItem>
                <asp:ListItem>1 Week</asp:ListItem>
                <asp:ListItem>2 Weeks</asp:ListItem>
                <asp:ListItem>3 Weeks</asp:ListItem>
                <asp:ListItem>4 Weeks</asp:ListItem>
                <asp:ListItem>1 Month</asp:ListItem>
                <asp:ListItem>2 Months</asp:ListItem>
                <asp:ListItem>3 Months</asp:ListItem>
                <asp:ListItem>4 Months</asp:ListItem>
                <asp:ListItem>5 Months</asp:ListItem>
                <asp:ListItem>1 Year</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="PanelNSDWide" runat="server" CssClass="auto-style7" Width="602px">
            <asp:Label ID="LabelPeriod1" runat="server" Text="Period 1"></asp:Label>
            &nbsp;
            <asp:DropDownList ID="DropDownListPeriod1" runat="server">
                <asp:ListItem>Current</asp:ListItem>
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelPeriod2" runat="server" Text="Period 2"></asp:Label>
&nbsp;
            <asp:DropDownList ID="DropDownListPeriod2" runat="server">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelPeriod3" runat="server" Text="Period 3"></asp:Label>
&nbsp;
            <asp:DropDownList ID="DropDownListPeriod3" runat="server">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelHeading1" runat="server" Text="Heading 1"></asp:Label>
            <asp:TextBox ID="TextBoxHeading1" runat="server" Width="55px" MaxLength="32"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="LabelHeading2" runat="server" Text="Heading 2"></asp:Label>
            <asp:TextBox ID="TextBoxHeading2" runat="server" Width="61px" MaxLength="32"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Label ID="LabelHeading3" runat="server" Text="Heading 3"></asp:Label>
            <asp:TextBox ID="TextBoxHeading3" runat="server" Width="57px" MaxLength="32"></asp:TextBox>
        </asp:Panel>
        &nbsp;<asp:Panel ID="PanelGraphView" runat="server" CssClass="auto-style10" Height="88px" Width="571px">
            <asp:Label ID="LabelCDLineColor" runat="server" Text="LineColor"></asp:Label>
            &nbsp;
            <asp:DropDownList ID="CDLineColor" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="LabelLegend" runat="server" Text="Legend"></asp:Label>
            &nbsp;&nbsp;<asp:TextBox ID="TextBoxLegend" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="LabelNumOfEvents" runat="server" Text="NumberOfEvents"></asp:Label>
            &nbsp;<asp:TextBox ID="NumOfEvents" runat="server" MaxLength="2" Width="32px">20</asp:TextBox>
            <asp:RangeValidator ID="NumOfEventsValidator" runat="server" ControlToValidate="NumOfEvents" ErrorMessage="Value cannot be more than 20" MaximumValue="20" MinimumValue="1" SetFocusOnError="True" ToolTip="Enter a number upto max of 20" Type="Integer"></asp:RangeValidator>
            <br />
            <asp:Label ID="LabelXAxis" runat="server" Text="X-axisLabel"></asp:Label>
            &nbsp;
            <asp:TextBox ID="TextBoxXAxis" runat="server"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Label ID="LabelYAxis" runat="server" Text="Y-axisLabel"></asp:Label>
            &nbsp;<asp:TextBox ID="TextBoxYAxis" runat="server"></asp:TextBox>
        </asp:Panel>
&nbsp;<br />
        <asp:Panel ID="PanelNSDQuadGraph" runat="server" CssClass="auto-style7" Height="162px" Width="1100px">
            <asp:Label ID="LabelNSDQuadGraphCellSource1" runat="server" Text="CellSource1"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphSource1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellDataType1" runat="server" Text="Cell Data Type1"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphType1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellLocation1" runat="server" Text="Location 1"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphLocation1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListLocation_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphAxisLabel1" runat="server" Text="X-Axis Label1"></asp:Label>
            <asp:TextBox ID="TextBoxQuadGraphXAxisLabel1" runat="server" MaxLength="32"></asp:TextBox>
            <asp:Label ID="LabelNSDQuadGraphLineColor1" runat="server" Text="Line Color"></asp:Label>
            <asp:DropDownList ID="DropDownListQuadGraphCDLineColor1" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelNSDQuadGraphCellSource2" runat="server" Text="CellSource2"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphSource2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellDataType2" runat="server" Text="Cell Data Type2"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphType2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellLocation2" runat="server" Text="Location 2"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphLocation2" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphAxisLabel2" runat="server" Text="X-Axis Label2"></asp:Label>
            <asp:TextBox ID="TextBoxQuadGraphXAxisLabel2" runat="server" MaxLength="32"></asp:TextBox>
            <asp:Label ID="LabelNSDQuadGraphLineColor2" runat="server" Text="Line Color"></asp:Label>
            <asp:DropDownList ID="DropDownListQuadGraphCDLineColor2" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelNSDQuadGraphCellSource3" runat="server" Text="CellSource3"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphSource3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellDataType3" runat="server" Text="Cell Data Type3"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphType3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellLocation3" runat="server" Text="Location 3"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphLocation3" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphAxisLabel3" runat="server" Text="X-Axis Label3"></asp:Label>
            <asp:TextBox ID="TextBoxQuadGraphXAxisLabel3" runat="server" MaxLength="32"></asp:TextBox>
            <asp:Label ID="LabelNSDQuadGraphLineColor3" runat="server" Text="Line Color"></asp:Label>
            <asp:DropDownList ID="DropDownListQuadGraphCDLineColor3" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelNSDQuadGraphCellSource4" runat="server" Text="CellSource4"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphSource4" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellDataType4" runat="server" Text="Cell Data Type4"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphType4" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphCellLocation4" runat="server" Text="Location 4"></asp:Label>
            <asp:DropDownList ID="DropDownListNSDQuadGraphLocation4" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="LabelNSDQuadGraphAxisLabel4" runat="server" Text="X-Axis Label4"></asp:Label>
            <asp:TextBox ID="TextBoxQuadGraphXAxisLabel4" runat="server" MaxLength="32"></asp:TextBox>
            <asp:Label ID="LabelNSDQuadGraphLineColor4" runat="server" Text="Line Color"></asp:Label>
            <asp:DropDownList ID="DropDownListQuadGraphCDLineColor4" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelNSDQuadGraphPeriod" runat="server" Text="Period 1"></asp:Label>
            &nbsp;&nbsp;<asp:DropDownList ID="DropDownListNSDQuadGraphPeriod" runat="server">
                <asp:ListItem>Current</asp:ListItem>
                <asp:ListItem>1 Day</asp:ListItem>
                <asp:ListItem>2 Days</asp:ListItem>
                <asp:ListItem>3 Days</asp:ListItem>
                <asp:ListItem>4 Days</asp:ListItem>
                <asp:ListItem>5 Days</asp:ListItem>
                <asp:ListItem>1 Week</asp:ListItem>
                <asp:ListItem>2 Weeks</asp:ListItem>
                <asp:ListItem>3 Weeks</asp:ListItem>
                <asp:ListItem>4 Weeks</asp:ListItem>
                <asp:ListItem>1 Month</asp:ListItem>
                <asp:ListItem>2 Months</asp:ListItem>
                <asp:ListItem>3 Months</asp:ListItem>
                <asp:ListItem>4 Months</asp:ListItem>
                <asp:ListItem>5 Months</asp:ListItem>
                <asp:ListItem>1 Year</asp:ListItem>
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="LabelNSDQuadGraphNumOfEvents" runat="server" Text="NumberOfEvents"></asp:Label>
            &nbsp;<asp:TextBox ID="QuadGraphNumOfEvents" runat="server" MaxLength="2" Width="32px">20</asp:TextBox>
            &nbsp;&nbsp;
            <asp:RangeValidator ID="NSDQuadGraphNumOfEventsValidator" runat="server" ControlToValidate="QuadGraphNumOfEvents" ErrorMessage="Value cannot be more than 20" MaximumValue="20" MinimumValue="1" SetFocusOnError="True" ToolTip="Enter a number upto max of 20" Type="Integer"></asp:RangeValidator>
            </asp:Panel>
        <br />
        <asp:HiddenField ID="XRefId" runat="server" />
        <br />
        <br />
        <br />
        Color<asp:DropDownList ID="CDColor" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
        </asp:DropDownList>
        <br />
        <br />
        Expiration date<asp:TextBox ID="ExpirationDate" runat="server" TextMode="Date"></asp:TextBox>
        <br />
        <br />
        Tags<asp:TextBox ID="Tags" runat="server"></asp:TextBox>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Panel ID="PanelImagePath" runat="server">
            <asp:Label ID="LabelImagePath" runat="server" Text="Image Path"></asp:Label>
            <asp:TextBox ID="ImagePath" runat="server" MaxLength="512"></asp:TextBox>
        </asp:Panel>
        <br />
        <asp:Panel ID="PanelStory" runat="server">
            <asp:Label ID="LabelStory" runat="server" Text="Story"></asp:Label>
            <asp:TextBox ID="Story" runat="server" MaxLength="1024"></asp:TextBox>
        </asp:Panel>
        <asp:Panel ID="PanelImageOverlay" runat="server">
            <br />
            <asp:Label ID="LabelImageOverlay" runat="server" Text="Image Overlay"></asp:Label>
            <asp:TextBox ID="ImageOverlay" runat="server" MaxLength="20"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="LabelOverlayColor" runat="server" Text="Overlay Color"></asp:Label>
            <asp:DropDownList ID="DropDownListOverlayColor" runat="server">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="LabelOverlayPosition" runat="server" Text="Overlay Position"></asp:Label>
            <asp:DropDownList ID="DropDownListOverlayPosition" runat="server">
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel ID="PanelURLPath" runat="server">
            <asp:Label ID="LabelURLPath" runat="server" Text="URL Path"></asp:Label>
            <asp:TextBox ID="URLPath" runat="server" MaxLength="512"></asp:TextBox>
        </asp:Panel>
        <asp:Label ID="LabelModalContent" runat="server" Text="Modal Content?"></asp:Label>
            &nbsp;
            <asp:DropDownList ID="DropDownListModalContent" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ModalContent_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
            </asp:DropDownList>
            <br />
        <asp:Panel ID="PanelAllModal" runat="server" Height="271px" style="margin-bottom: 0px">
            <asp:Label ID="LabelModalLongTitle" runat="server" Text="Modal Long Title"></asp:Label>
            &nbsp;<asp:TextBox ID="TextBoxModalLongTitle" runat="server" MaxLength="60"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalDetailNotes" runat="server" Text="Modal Detail Notes"></asp:Label>
            &nbsp;<asp:TextBox ID="TextBoxModalDetailNotes" runat="server" MaxLength="120" Width="243px"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalInput" runat="server" Text="Modal Input"></asp:Label>
            &nbsp;<asp:DropDownList ID="DropDownListModalInput" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="True">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelModalTypeID" runat="server" Text="ModalTypeID"></asp:Label>
            &nbsp;<asp:DropDownList ID="DropDownListModalType" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="True">
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelModalContentType" runat="server" Text="ModalContentType"></asp:Label>
            &nbsp;<asp:DropDownList ID="DropDownListModalContentType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ModalContentType_SelectedIndexChanged">
                <asp:ListItem Value="webView">WebView</asp:ListItem>
                <asp:ListItem>Graph</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelModalTextOverlay" runat="server" Text="Modal Text Overlay"></asp:Label>
            &nbsp;<asp:TextBox ID="TextBoxModalTextOverlay" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalImageNewsPath" runat="server" Text="Modal Image News Path"></asp:Label>
            &nbsp;<asp:TextBox ID="TextBoxModalImageNewsPath" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalCRIndicator" runat="server" Text="Modal CR Indicator"></asp:Label>
            &nbsp;<asp:DropDownList ID="DropDownListModalCR" runat="server">
                <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="LabelModalGraphType" runat="server" Text="Modal Graph Type"></asp:Label>
            &nbsp;<asp:DropDownList ID="DropDownListModalGraphType" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="True">
            </asp:DropDownList>
            <br />&nbsp;&nbsp;&nbsp;&nbsp;</asp:Panel>
        <asp:Panel ID="PanelModalChart" runat="server" Height="159px" Width="1320px">
            <asp:Label ID="ModalLabel" runat="server" Font-Bold="True" Text="Modal"></asp:Label>
            <br />
            <asp:Label ID="LabelModalCellSource1" runat="server" Text="Source1"></asp:Label>
            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DropDownListModalCellSource1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="LabelModalCellDataType1" runat="server" Text="DataType1"></asp:Label>
            <asp:DropDownList ID="DropDownListModalDataType1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelModalCellLocation1" runat="server" Text="Location1"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLocation1" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="ModalPeriod1" runat="server" Text="Period1"></asp:Label>
            <asp:DropDownList ID="DropDownListModalPeriod1" runat="server">
                <asp:ListItem>Current</asp:ListItem>
                <asp:ListItem>1 Day</asp:ListItem>
                <asp:ListItem>2 Days</asp:ListItem>
                <asp:ListItem>3 Days</asp:ListItem>
                <asp:ListItem>4 Days</asp:ListItem>
                <asp:ListItem>5 Days</asp:ListItem>
                <asp:ListItem>1 Week</asp:ListItem>
                <asp:ListItem>2 Weeks</asp:ListItem>
                <asp:ListItem>3 Weeks</asp:ListItem>
                <asp:ListItem>4 Weeks</asp:ListItem>
                <asp:ListItem>1 Month</asp:ListItem>
                <asp:ListItem>2 Months</asp:ListItem>
                <asp:ListItem>3 Months</asp:ListItem>
                <asp:ListItem>4 Months</asp:ListItem>
                <asp:ListItem>5 Months</asp:ListItem>
                <asp:ListItem>1 Year</asp:ListItem>
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelModalNumofEvents1" runat="server" Text="#Events1"></asp:Label>
            <asp:TextBox ID="TextBoxModalNumOfEvents1" runat="server" Width="86px"></asp:TextBox>
            &nbsp;
            <asp:Label ID="LabelModalLinecolor1" runat="server" Text="Linecolor1"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLineColor1" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelModalLabel1" runat="server" Text="LabelText1"></asp:Label>
            <asp:TextBox ID="TextBoxModalLabel1" runat="server" CssClass="auto-style11" Width="77px"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalCellSource2" runat="server" Text="Source2"></asp:Label>
            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DropDownListModalCellSource2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="LabelModalCellDataType2" runat="server" Text="DataType2"></asp:Label>
            <asp:DropDownList ID="DropDownListModalDataType2" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelModalCellLocation2" runat="server" Text="Location2"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLocation2" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="ModalPeriod2" runat="server" Text="Period2"></asp:Label>
            <asp:DropDownList ID="DropDownListModalPeriod2" runat="server">
                <asp:ListItem>Current</asp:ListItem>
                <asp:ListItem>1 Day</asp:ListItem>
                <asp:ListItem>2 Days</asp:ListItem>
                <asp:ListItem>3 Days</asp:ListItem>
                <asp:ListItem>4 Days</asp:ListItem>
                <asp:ListItem>5 Days</asp:ListItem>
                <asp:ListItem>1 Week</asp:ListItem>
                <asp:ListItem>2 Weeks</asp:ListItem>
                <asp:ListItem>3 Weeks</asp:ListItem>
                <asp:ListItem>4 Weeks</asp:ListItem>
                <asp:ListItem>1 Month</asp:ListItem>
                <asp:ListItem>2 Months</asp:ListItem>
                <asp:ListItem>3 Months</asp:ListItem>
                <asp:ListItem>4 Months</asp:ListItem>
                <asp:ListItem>5 Months</asp:ListItem>
                <asp:ListItem>1 Year</asp:ListItem>
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelModalNumofEvents2" runat="server" Text="#Events2"></asp:Label>
            <asp:TextBox ID="TextBoxModalNumOfEvents2" runat="server" Width="86px"></asp:TextBox>
&nbsp;
            <asp:Label ID="LabelModalLinecolor2" runat="server" Text="Linecolor2"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLineColor2" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelModalLabel2" runat="server" Text="LabelText2"></asp:Label>
            <asp:TextBox ID="TextBoxModalLabel2" runat="server" CssClass="auto-style11" Width="77px"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalCellSource3" runat="server" Text="Source3"></asp:Label>
            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DropDownListModalCellSource3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="LabelModalCellDataType3" runat="server" Text="DataType3"></asp:Label>
            <asp:DropDownList ID="DropDownListModalDataType3" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelModalCellLocation3" runat="server" Text="Location3"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLocation3" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="ModalPeriod3" runat="server" Text="Period3"></asp:Label>
            <asp:DropDownList ID="DropDownListModalPeriod3" runat="server">
                <asp:ListItem>Current</asp:ListItem>
                <asp:ListItem>1 Day</asp:ListItem>
                <asp:ListItem>2 Days</asp:ListItem>
                <asp:ListItem>3 Days</asp:ListItem>
                <asp:ListItem>4 Days</asp:ListItem>
                <asp:ListItem>5 Days</asp:ListItem>
                <asp:ListItem>1 Week</asp:ListItem>
                <asp:ListItem>2 Weeks</asp:ListItem>
                <asp:ListItem>3 Weeks</asp:ListItem>
                <asp:ListItem>4 Weeks</asp:ListItem>
                <asp:ListItem>1 Month</asp:ListItem>
                <asp:ListItem>2 Months</asp:ListItem>
                <asp:ListItem>3 Months</asp:ListItem>
                <asp:ListItem>4 Months</asp:ListItem>
                <asp:ListItem>5 Months</asp:ListItem>
                <asp:ListItem>1 Year</asp:ListItem>
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelModalNumofEvents3" runat="server" Text="#Events3"></asp:Label>
            <asp:TextBox ID="TextBoxModalNumOfEvents3" runat="server" Width="86px"></asp:TextBox>
&nbsp;
            <asp:Label ID="LabelModalLinecolor3" runat="server" Text="Linecolor3"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLineColor3" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelModalLabel3" runat="server" Text="LabelText3"></asp:Label>
            <asp:TextBox ID="TextBoxModalLabel3" runat="server" CssClass="auto-style11" Width="77px"></asp:TextBox>
            <br />
            <asp:Label ID="LabelModalCellSource4" runat="server" Text="Source4"></asp:Label>
            &nbsp;&nbsp;
            <asp:DropDownList ID="DropDownListModalCellSource4" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListCellSource_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;
            <asp:Label ID="LabelModalCellDataType4" runat="server" Text="DataType4"></asp:Label>
            <asp:DropDownList ID="DropDownListModalDataType4" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="GenericDropDownListDataType_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="LabelModalCellLocation4" runat="server" Text="Location4"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLocation4" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="ModalPeriod4" runat="server" Text="Period4"></asp:Label>
            <asp:DropDownList ID="DropDownListModalPeriod4" runat="server">
                <asp:ListItem>Current</asp:ListItem>
                <asp:ListItem>1 Day</asp:ListItem>
                <asp:ListItem>2 Days</asp:ListItem>
                <asp:ListItem>3 Days</asp:ListItem>
                <asp:ListItem>4 Days</asp:ListItem>
                <asp:ListItem>5 Days</asp:ListItem>
                <asp:ListItem>1 Week</asp:ListItem>
                <asp:ListItem>2 Weeks</asp:ListItem>
                <asp:ListItem>3 Weeks</asp:ListItem>
                <asp:ListItem>4 Weeks</asp:ListItem>
                <asp:ListItem>1 Month</asp:ListItem>
                <asp:ListItem>2 Months</asp:ListItem>
                <asp:ListItem>3 Months</asp:ListItem>
                <asp:ListItem>4 Months</asp:ListItem>
                <asp:ListItem>5 Months</asp:ListItem>
                <asp:ListItem>1 Year</asp:ListItem>
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelModalNumofEvents4" runat="server" Text="#Events4"></asp:Label>
            <asp:TextBox ID="TextBoxModalNumOfEvents4" runat="server" Width="86px"></asp:TextBox>
&nbsp;
            <asp:Label ID="LabelModalLinecolor4" runat="server" Text="Linecolor4"></asp:Label>
            <asp:DropDownList ID="DropDownListModalLineColor4" runat="server" AppendDataBoundItems="True" DataMember="DefaultView" DataSourceID="CDMDev">
            </asp:DropDownList>
&nbsp;
            <asp:Label ID="LabelModalLabel4" runat="server" Text="LabelText4"></asp:Label>
            <asp:TextBox ID="TextBoxModalLabel4" runat="server" CssClass="auto-style11" Width="77px"></asp:TextBox>
            <br />
            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        </asp:Panel>
        <p>
        <asp:Button ID="SaveRefresh" runat="server" OnClick="SaveRefresh_Click" Text="Save And Refresh" CssClass="rounded-button" Width="145px" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Save" runat="server" Text="Save" OnClick="Save_Click" CssClass="rounded-button" Width="89px" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Clear" runat="server" CssClass="rounded-button" Text="Clear" OnClick="Clear_Click" Width="89px" CausesValidation="False" />
        </p>
    </form>
        
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
        
</body>
</html>
