﻿using CDMManager;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

namespace CDMManager
{
    public partial class CellDefinition : System.Web.UI.Page
    {
        private void listSourceBind(SqlConnection sql, String table, String idColumn, String valueColumn, DropDownList dropDownList)
        {

            string sqlGetDataSource = "select " + idColumn + ", " + valueColumn + " from " + table;
            SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
            adapter.Fill(dataTable);
            dropDownList.DataSource = dataTable;
            dropDownList.DataSourceID = null;
            dropDownList.DataValueField = idColumn.ToString();
            dropDownList.DataTextField = valueColumn.ToString();
            dropDownList.DataBind();
            dropDownList.Items.Insert(0, new ListItem("Select", "0"));
        }

        private void BindDropDownList(SqlConnection con, DropDownList ddl, string query, string text, string value, string defaultText)
        {
            SqlCommand cmd = new SqlCommand(query);
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;
                con.Open();
                ddl.DataSource = cmd.ExecuteReader();
                ddl.DataSourceID = null;
                ddl.DataTextField = text;
                ddl.DataValueField = value;
                ddl.DataBind();
                con.Close();
            }
            ddl.Items.Insert(0, new ListItem(defaultText, "0"));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                String mode = Request["mode"];

                using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                {
                    listSourceBind(sql, "[Cdef].[CellInput]", "CDInputID", "InputDesc", CDInput);
                    listSourceBind(sql, "[Cdef].[CellLayout]", "CDLayoutID", "LayoutDesc", CDLayout);
                    listSourceBind(sql, "[Cdef].[CellType]", "CDCellTypeID", "CellTypeDesc", CDCellType);
                    listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", CDColor);
                    listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", CDLineColor);
                    listSourceBind(sql, "[Cdef].[OverlayColor]", "ImageOverlayColorID", "ImageOverlayColorDesc", DropDownListOverlayColor);
                    listSourceBind(sql, "[Cdef].[OverlayPosition]", "ImageOverlayPositionID", "ImageOverlayPositionDesc", DropDownListOverlayPosition);
                    listSourceBind(sql, "[Cdef].[CellInput]", "CDInputID", "InputDesc", DropDownListModalInput);
                    listSourceBind(sql, "[Cdef].[CellLayout]", "CDLayoutID", "LayoutDesc", DropDownListModalType);
                    listSourceBind(sql, "[Cdef].[ModalGraphType]", "CDModalGraphTypeID", "ModalGraphTypeDesc", DropDownListModalGraphType);
                    listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListModalLineColor1);
                    listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListModalLineColor2);
                    listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListModalLineColor3);
                    listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListModalLineColor4);
                    for (int i = 0; i < PanelImagePath.Controls.Count; i++)
                        PanelImagePath.Controls[i].Visible = false;
                    for (int i = 0; i < PanelNSDSingle.Controls.Count; i++)
                        PanelNSDSingle.Controls[i].Visible = false;
                    for (int i = 0; i < PanelNSDWide.Controls.Count; i++)
                        PanelNSDWide.Controls[i].Visible = false;
                    for (int i = 0; i < PanelImageOverlay.Controls.Count; i++)
                        PanelImageOverlay.Controls[i].Visible = false;
                    for (int i = 0; i < PanelNSDTall.Controls.Count; i++)
                        PanelNSDTall.Controls[i].Visible = false;
                    for (int i = 0; i < PanelNSDTallExclusive.Controls.Count; i++)
                        PanelNSDTallExclusive.Controls[i].Visible = false;
                    for (int i = 0; i < PanelNSDQuadGraph.Controls.Count; i++)
                        PanelNSDQuadGraph.Controls[i].Visible = false;
                    for (int i = 0; i < PanelStory.Controls.Count; i++)
                        PanelStory.Controls[i].Visible = false;
                    for (int i = 0; i < PanelURLPath.Controls.Count; i++)
                        PanelURLPath.Controls[i].Visible = false;
                    for (int i = 0; i < PanelGraphView.Controls.Count; i++)
                        PanelGraphView.Controls[i].Visible = false;
                    for (int i = 0; i < PanelAllModal.Controls.Count; i++)
                        PanelAllModal.Controls[i].Visible = false;
                    for (int i = 0; i < PanelModalChart.Controls.Count; i++)
                        PanelModalChart.Controls[i].Visible = false;

                    if (!String.IsNullOrEmpty(mode) && mode.Equals("edit"))
                    {
                        PrepareForEdit();
                    }
                    else
                    {
                    }

                    sql.Close();
                }
            }
        }

        protected void PrepareForEdit()
        {
            DataTable cellDefinitionTable = new DataTable();
            String cellDefId = Request["celldefinitionId"];
            using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
            {
                cellDefinitionTable = Utilities.RunSql(sql, "select * from Cdef.CellDefinition where Id = " + cellDefId);
                if (cellDefinitionTable.Rows.Count == 1)
                {
                    CellId.Text = cellDefinitionTable.Select("").FirstOrDefault()["Id"].ToString();
                    Status.Items.FindByValue(Convert.ToInt32(cellDefinitionTable.Select("").FirstOrDefault()["CellStatus"]).ToString()).Selected = true;
                    Ad.Items.FindByValue(Convert.ToInt32(cellDefinitionTable.Select("").FirstOrDefault()["IsAd"]).ToString()).Selected = true;
                    Title.Text = cellDefinitionTable.Select("").FirstOrDefault()["Title"].ToString();
                    LongTitle.Text = cellDefinitionTable.Select("").FirstOrDefault()["LongTitle"].ToString();
                    CellDescription.Text = cellDefinitionTable.Select("").FirstOrDefault()["CellDescription"].ToString();
                    CDInput.Items.FindByValue(cellDefinitionTable.Select("").FirstOrDefault()["CDInputID"].ToString()).Selected = true;
                    CDLayout.Items.FindByValue(cellDefinitionTable.Select("").FirstOrDefault()["CDLayoutID"].ToString()).Selected = true;
                    CDCellType.Items.FindByValue(cellDefinitionTable.Select("").FirstOrDefault()["CDCellTypeId"].ToString()).Selected = true;
                    CDCellType_SelectedIndexChanged(CDCellType, EventArgs.Empty);
                    if (!String.IsNullOrEmpty(cellDefinitionTable.Select("").FirstOrDefault()["CDColorID"].ToString()))
                        CDColor.Items.FindByValue(cellDefinitionTable.Select("").FirstOrDefault()["CDColorID"].ToString()).Selected = true;
                    if (!String.IsNullOrEmpty(cellDefinitionTable.Select("").FirstOrDefault()["ExpirationDate"].ToString()))
                        ExpirationDate.Text = Convert.ToDateTime(cellDefinitionTable.Select("").FirstOrDefault()["ExpirationDate"]).ToString("MM/dd/yyyy");
                    Tags.Text = cellDefinitionTable.Select("").FirstOrDefault()["Tags"].ToString();
                    ImagePath.Text = cellDefinitionTable.Select("").FirstOrDefault()["ImagePath"].ToString();
                    DropDownListModalContent.ClearSelection();
                    DropDownListModalContent.Items.FindByValue(Convert.ToInt32(cellDefinitionTable.Select("").FirstOrDefault()["IsModal"]).ToString()).Selected = true;
                    ModalContentType_SelectedIndexChanged(DropDownListModalContent, EventArgs.Empty);
                    setPanelVisibility(CDCellType);
                    
                    if (CDInput.SelectedItem.ToString().Equals("NSD"))
                    {
                        DataTable cellSourceTable = new DataTable();
                        cellSourceTable = Utilities.RunSql(sql, "select * from Cdef.CellSource where CellDefinitionId = " + cellDefId);
                        for (int i = 1; i <= cellSourceTable.Rows.Count; i++)
                        {
                            if ((CDInput.SelectedItem.ToString().Equals("NSD") &&
                                (CDLayout.SelectedItem.ToString().Equals("Single") || CDLayout.SelectedItem.ToString().Equals("Wide"))) ||
                                (CDInput.SelectedItem.ToString().Equals("NSD") && CDCellType.SelectedItem.ToString().Equals("GraphView") &&
                                (CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Wide"))))
                            {
                                DropDownListCellSource.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["CellSource"].ToString()).Selected = true;
                                DropDownListCellSource_SelectedIndexChanged(DropDownListCellSource, EventArgs.Empty);
                                DropDownListDataType.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["CellDataType"].ToString()).Selected = true;
                                DropDownListDataType_SelectedIndexChanged(DropDownListDataType, EventArgs.Empty);
                                DropDownListLocation.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["CellLocation"].ToString()).Selected = true;
                                DropDownListLocation_SelectedIndexChanged(DropDownListLocation, EventArgs.Empty);
                            }
                            if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                               (CDLayout.SelectedItem.ToString().Equals("Wide") || CDLayout.SelectedItem.ToString().Equals("Quad")) &&
                               (!CDCellType.SelectedItem.ToString().Equals("GraphView")))
                            {
                                DropDownListPeriod1.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["Period1"].ToString()).Selected = true;
                                DropDownListPeriod2.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["Period2"].ToString()).Selected = true;
                                DropDownListPeriod3.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["Period3"].ToString()).Selected = true;
                                TextBoxHeading1.Text = cellSourceTable.Select("").FirstOrDefault()["Heading1"].ToString();
                                TextBoxHeading2.Text = cellSourceTable.Select("").FirstOrDefault()["Heading2"].ToString();
                                TextBoxHeading3.Text = cellSourceTable.Select("").FirstOrDefault()["Heading3"].ToString();
                            }
                            if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                                (CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Quad")) &&
                                (!CDCellType.SelectedItem.ToString().Equals("GraphView")))
                            {
                                if (i == 1)
                                {
                                    DropDownListNSDTallSource1.Items.FindByValue(cellSourceTable.Rows[i-1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDTallSource1, EventArgs.Empty);
                                    DropDownListNSDTallType1.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDTallType1, EventArgs.Empty);
                                    DropDownListNSDTallLocation1.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxXAxisLabel1.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                }
                                else if (i == 2)
                                {
                                    DropDownListNSDTallSource2.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDTallSource2, EventArgs.Empty);
                                    DropDownListNSDTallType2.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDTallType2, EventArgs.Empty);
                                    DropDownListNSDTallLocation2.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxXAxisLabel2.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                }
                                else if (i == 3)
                                {
                                    DropDownListNSDTallSource3.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDTallSource3, EventArgs.Empty);
                                    DropDownListNSDTallType3.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDTallType3, EventArgs.Empty);
                                    DropDownListNSDTallLocation3.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxXAxisLabel3.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                }
                                else if (i == 4)
                                {
                                    DropDownListNSDTallSource4.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDTallSource4, EventArgs.Empty);
                                    DropDownListNSDTallType4.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDTallType4, EventArgs.Empty);
                                    DropDownListNSDTallLocation4.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxXAxisLabel4.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                }

                                if (CDLayout.SelectedItem.ToString().Equals("Tall"))
                                {
                                    DropDownListNSDTallPeriod.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["Period1"].ToString()).Selected = true;
                                    NSDTallHeading.Text = cellSourceTable.Select("").FirstOrDefault()["Heading1"].ToString();
                                }
                            }
                            else if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                                    CDLayout.SelectedItem.ToString().Equals("Tall") &&
                                    CDCellType.SelectedItem.ToString().Equals("GraphView"))
                            {
                                DropDownListNSDTallPeriod.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["Period1"].ToString()).Selected = true;
                                NSDTallHeading.Text = cellSourceTable.Select("").FirstOrDefault()["Heading1"].ToString();
                            }
                            if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Quad") && CDCellType.SelectedItem.ToString().Equals("GraphView"))
                            {
                                if (i == 1)
                                {
                                    DropDownListNSDQuadGraphSource1.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDQuadGraphSource1, EventArgs.Empty);
                                    DropDownListNSDQuadGraphType1.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDQuadGraphType1, EventArgs.Empty);
                                    DropDownListNSDQuadGraphLocation1.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxQuadGraphXAxisLabel1.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                    DropDownListQuadGraphCDLineColor1.Items.FindByValue(cellSourceTable.Rows[i - 1]["LineColor"].ToString()).Selected = true;
                                }
                                else if (i == 2)
                                {
                                    DropDownListNSDQuadGraphSource2.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDQuadGraphSource2, EventArgs.Empty);
                                    DropDownListNSDQuadGraphType2.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDQuadGraphType2, EventArgs.Empty);
                                    DropDownListNSDQuadGraphLocation2.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxQuadGraphXAxisLabel2.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                    DropDownListQuadGraphCDLineColor2.Items.FindByValue(cellSourceTable.Rows[i - 1]["LineColor"].ToString()).Selected = true;
                                }
                                else if (i == 3)
                                {
                                    DropDownListNSDQuadGraphSource3.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDQuadGraphSource3, EventArgs.Empty);
                                    DropDownListNSDQuadGraphType3.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDQuadGraphType3, EventArgs.Empty);
                                    DropDownListNSDQuadGraphLocation3.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxQuadGraphXAxisLabel3.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                    DropDownListQuadGraphCDLineColor3.Items.FindByValue(cellSourceTable.Rows[i - 1]["LineColor"].ToString()).Selected = true;
                                }
                                else if (i ==4)
                                {
                                    DropDownListNSDQuadGraphSource4.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListNSDQuadGraphSource4, EventArgs.Empty);
                                    DropDownListNSDQuadGraphType4.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListNSDQuadGraphType4, EventArgs.Empty);
                                    DropDownListNSDQuadGraphLocation4.Items.FindByValue(cellSourceTable.Rows[i - 1]["CellLocation"].ToString()).Selected = true;
                                    TextBoxQuadGraphXAxisLabel4.Text = cellSourceTable.Rows[i - 1]["XaxisLabel"].ToString();
                                    DropDownListQuadGraphCDLineColor4.Items.FindByValue(cellSourceTable.Rows[i - 1]["LineColor"].ToString()).Selected = true;
                                }
                            }
                            if (CDCellType.SelectedItem.ToString().Equals("GraphView") && !CDLayout.SelectedItem.ToString().Equals("Quad"))
                            {
                                CDLineColor.Items.FindByValue(cellSourceTable.Select("").FirstOrDefault()["LineColor"].ToString()).Selected = true;
                                TextBoxLegend.Text = cellSourceTable.Select("").FirstOrDefault()["Legend"].ToString();
                                NumOfEvents.Text = cellSourceTable.Select("").FirstOrDefault()["NumberOfEvents"].ToString();
                                TextBoxXAxis.Text = cellSourceTable.Select("").FirstOrDefault()["XaxisLabel"].ToString();
                                TextBoxYAxis.Text = cellSourceTable.Select("").FirstOrDefault()["YaxisLabel"].ToString();
                            }
                        }
                    }

                    if (DropDownListModalContent.SelectedItem.ToString().Equals("Yes"))
                    {
                        DataTable modalContentTable = new DataTable();
                        modalContentTable = Utilities.RunSql(sql, "select * from Cdef.ModalContent where CellDefinitionId = " + cellDefId);

                        for (int i = 1; i <= modalContentTable.Rows.Count; i++)
                        {
                            TextBoxModalLongTitle.Text = modalContentTable.Select("").FirstOrDefault()["ModalLongTitle"].ToString();
                            TextBoxModalDetailNotes.Text = modalContentTable.Select("").FirstOrDefault()["ModalDetailNotes"].ToString();
                            DropDownListModalInput.Items.FindByValue(modalContentTable.Select("").FirstOrDefault()["ModalInputID"].ToString()).Selected = true;
                            DropDownListModalType.Items.FindByValue(modalContentTable.Select("").FirstOrDefault()["ModalTypeID"].ToString()).Selected = true;
                            DropDownListModalContentType.ClearSelection();
                            DropDownListModalContentType.Items.FindByValue(modalContentTable.Select("").FirstOrDefault()["ModalContentType"].ToString()).Selected = true;
                            TextBoxModalTextOverlay.Text = modalContentTable.Select("").FirstOrDefault()["ModalTextOverlay"].ToString();
                            TextBoxModalImageNewsPath.Text = modalContentTable.Select("").FirstOrDefault()["ModalImageNewsPath"].ToString();
                            DropDownListModalCR.Items.FindByValue(Convert.ToInt32(modalContentTable.Select("").FirstOrDefault()["ModalCRIndicator"]).ToString()).Selected = true;
                            if (!String.IsNullOrEmpty(modalContentTable.Select("").FirstOrDefault()["ModalGraphTypeID"].ToString()))
                                DropDownListModalGraphType.Items.FindByValue(modalContentTable.Select("").FirstOrDefault()["ModalGraphTypeID"].ToString()).Selected = true;
                        }

                        if (DropDownListModalContentType.SelectedValue.ToString().Equals("Graph"))
                        {
                            setPanelVisibility(DropDownListModalContentType);
                            DataTable modalSourceTable = new DataTable();
                            modalSourceTable = Utilities.RunSql(sql, "select * from Cdef.ModalSources where CellDefinitionId = " + cellDefId);

                            for (int i = 1; i <= modalSourceTable.Rows.Count; i++)
                            {
                                if (i == 1)
                                {
                                    DropDownListModalCellSource1.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListModalCellSource1, EventArgs.Empty);
                                    DropDownListModalDataType1.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListModalDataType1, EventArgs.Empty);
                                    DropDownListModalLocation1.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellLocation"].ToString()).Selected = true;
                                    DropDownListModalPeriod1.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalPeriod"].ToString()).Selected = true;
                                    TextBoxModalNumOfEvents1.Text = modalSourceTable.Rows[i - 1]["ModalNumOfEvents"].ToString();
                                    TextBoxModalLabel1.Text = modalSourceTable.Rows[i - 1]["ModalLabel"].ToString();
                                    DropDownListModalLineColor1.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalLineColor"].ToString()).Selected = true;
                                }
                                else if (i == 2)
                                {
                                    DropDownListModalCellSource2.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListModalCellSource2, EventArgs.Empty);
                                    DropDownListModalDataType2.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListModalDataType2, EventArgs.Empty);
                                    DropDownListModalLocation2.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellLocation"].ToString()).Selected = true;
                                    DropDownListModalPeriod2.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalPeriod"].ToString()).Selected = true;
                                    TextBoxModalNumOfEvents2.Text = modalSourceTable.Rows[i - 1]["ModalNumOfEvents"].ToString();
                                    TextBoxModalLabel2.Text = modalSourceTable.Rows[i - 1]["ModalLabel"].ToString();
                                    DropDownListModalLineColor2.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalLineColor"].ToString()).Selected = true;
                                }
                                else if (i == 3)
                                {
                                    DropDownListModalCellSource3.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListModalCellSource3, EventArgs.Empty);
                                    DropDownListModalDataType3.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListModalDataType3, EventArgs.Empty);
                                    DropDownListModalLocation3.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellLocation"].ToString()).Selected = true;
                                    DropDownListModalPeriod3.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalPeriod"].ToString()).Selected = true;
                                    TextBoxModalNumOfEvents3.Text = modalSourceTable.Rows[i - 1]["ModalNumOfEvents"].ToString();
                                    TextBoxModalLabel3.Text = modalSourceTable.Rows[i - 1]["ModalLabel"].ToString();
                                    DropDownListModalLineColor3.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalLineColor"].ToString()).Selected = true;
                                }
                                else if (i == 4)
                                {
                                    DropDownListModalCellSource4.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellSource"].ToString()).Selected = true;
                                    GenericDropDownListCellSource_SelectedIndexChanged(DropDownListModalCellSource4, EventArgs.Empty);
                                    DropDownListModalDataType4.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellDataType"].ToString()).Selected = true;
                                    GenericDropDownListDataType_SelectedIndexChanged(DropDownListModalDataType4, EventArgs.Empty);
                                    DropDownListModalLocation4.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalCellLocation"].ToString()).Selected = true;
                                    DropDownListModalPeriod4.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalPeriod"].ToString()).Selected = true;
                                    TextBoxModalNumOfEvents4.Text = modalSourceTable.Rows[i - 1]["ModalNumOfEvents"].ToString();
                                    TextBoxModalLabel4.Text = modalSourceTable.Rows[i - 1]["ModalLabel"].ToString();
                                    DropDownListModalLineColor4.Items.FindByValue(modalSourceTable.Rows[i - 1]["ModalLineColor"].ToString()).Selected = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void Status_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void setPanelVisibility(Object sender)
        {
            DropDownList selectedDropDown = sender as DropDownList;
            
            if (DropDownListModalContent.SelectedItem.ToString().Equals("Yes"))
            {
                for (int i = 0; i < PanelAllModal.Controls.Count; i++)
                    PanelAllModal.Controls[i].Visible = true;

                if (DropDownListModalContentType.SelectedItem.ToString().Equals("Graph"))
                {
                    for (int i = 0; i < PanelModalChart.Controls.Count; i++)
                        PanelModalChart.Controls[i].Visible = true;

                    if (CDInput.SelectedItem.ToString().Equals("Other"))
                    {
                        using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                        {
                            String SourceQuery = "select distinct NSDSourceID, NSDSource from Cdef.NSDXRef";
                            DropDownListModalCellSource1.Items.Clear();
                            DropDownListModalDataType1.Items.Clear();
                            DropDownListModalLocation1.Items.Clear();
                            DropDownListModalCellSource2.Items.Clear();
                            DropDownListModalDataType2.Items.Clear();
                            DropDownListModalLocation2.Items.Clear();
                            DropDownListModalCellSource3.Items.Clear();
                            DropDownListModalDataType3.Items.Clear();
                            DropDownListModalLocation3.Items.Clear();
                            DropDownListModalCellSource4.Items.Clear();
                            DropDownListModalDataType4.Items.Clear();
                            DropDownListModalLocation4.Items.Clear();
                            BindDropDownList(sql, DropDownListModalCellSource1, SourceQuery, "NSDSource", "NSDSourceID", "Select Source");
                            DropDownListModalCellSource2.Items.AddRange(DropDownListModalCellSource1.Items.OfType<ListItem>().ToArray());
                            DropDownListModalCellSource3.Items.AddRange(DropDownListModalCellSource1.Items.OfType<ListItem>().ToArray());
                            DropDownListModalCellSource4.Items.AddRange(DropDownListModalCellSource1.Items.OfType<ListItem>().ToArray());
                            sql.Close();
                        }
                    }
                    else
                    {
                        DropDownListModalCellSource1.Items.Clear();
                        DropDownListModalDataType1.Items.Clear();
                        DropDownListModalLocation1.Items.Clear();
                        if ((CDInput.SelectedItem.ToString().Equals("NSD") &&
                            (CDLayout.SelectedItem.ToString().Equals("Single") || CDLayout.SelectedItem.ToString().Equals("Wide"))) ||
                            (CDInput.SelectedItem.ToString().Equals("NSD") && CDCellType.SelectedItem.ToString().Equals("GraphView") &&
                            (CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Wide"))))
                        {
                            DropDownListModalCellSource1.Items.AddRange(DropDownListCellSource.Items.OfType<ListItem>().ToArray());
                            DropDownListModalDataType1.Items.AddRange(DropDownListDataType.Items.OfType<ListItem>().ToArray());
                            DropDownListModalLocation1.Items.AddRange(DropDownListLocation.Items.OfType<ListItem>().ToArray());
                            DropDownListModalCellSource2.Items.AddRange(DropDownListCellSource.Items.OfType<ListItem>().ToArray());
                            DropDownListModalDataType2.Items.AddRange(DropDownListDataType.Items.OfType<ListItem>().ToArray());
                            DropDownListModalLocation2.Items.AddRange(DropDownListLocation.Items.OfType<ListItem>().ToArray());
                            DropDownListModalCellSource3.Items.AddRange(DropDownListCellSource.Items.OfType<ListItem>().ToArray());
                            DropDownListModalDataType3.Items.AddRange(DropDownListDataType.Items.OfType<ListItem>().ToArray());
                            DropDownListModalLocation3.Items.AddRange(DropDownListLocation.Items.OfType<ListItem>().ToArray());
                            DropDownListModalCellSource4.Items.AddRange(DropDownListCellSource.Items.OfType<ListItem>().ToArray());
                            DropDownListModalDataType4.Items.AddRange(DropDownListDataType.Items.OfType<ListItem>().ToArray());
                            DropDownListModalLocation4.Items.AddRange(DropDownListLocation.Items.OfType<ListItem>().ToArray());
                        }
                        else if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                            (CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Quad")) &&
                            (!CDCellType.SelectedItem.ToString().Equals("GraphView") && !CDCellType.SelectedItem.ToString().Equals("Select")))
                        {
                            if (DropDownListNSDTallSource1.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource1.Items.AddRange(DropDownListNSDTallSource1.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType1.Items.AddRange(DropDownListNSDTallType1.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation1.Items.AddRange(DropDownListNSDTallLocation1.Items.OfType<ListItem>().ToArray());
                            }
                            if (DropDownListNSDTallSource2.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource2.Items.AddRange(DropDownListNSDTallSource2.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType2.Items.AddRange(DropDownListNSDTallType2.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation2.Items.AddRange(DropDownListNSDTallLocation2.Items.OfType<ListItem>().ToArray());
                            }
                            if (DropDownListNSDTallSource3.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource3.Items.AddRange(DropDownListNSDTallSource3.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType3.Items.AddRange(DropDownListNSDTallType3.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation3.Items.AddRange(DropDownListNSDTallLocation3.Items.OfType<ListItem>().ToArray());
                            }
                            if (DropDownListNSDTallSource4.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource4.Items.AddRange(DropDownListNSDTallSource4.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType4.Items.AddRange(DropDownListNSDTallType4.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation4.Items.AddRange(DropDownListNSDTallLocation4.Items.OfType<ListItem>().ToArray());
                            }
                        }
                        else if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Quad") && CDCellType.SelectedItem.ToString().Equals("GraphView"))
                        {
                            if (DropDownListNSDQuadGraphSource1.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource1.Items.AddRange(DropDownListNSDQuadGraphSource1.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType1.Items.AddRange(DropDownListNSDQuadGraphType1.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation1.Items.AddRange(DropDownListNSDQuadGraphLocation1.Items.OfType<ListItem>().ToArray());
                            }
                            if (DropDownListNSDQuadGraphSource2.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource2.Items.AddRange(DropDownListNSDQuadGraphSource2.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType2.Items.AddRange(DropDownListNSDQuadGraphType2.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation2.Items.AddRange(DropDownListNSDQuadGraphLocation2.Items.OfType<ListItem>().ToArray());
                            }
                            if (DropDownListNSDQuadGraphSource3.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource3.Items.AddRange(DropDownListNSDQuadGraphSource3.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType3.Items.AddRange(DropDownListNSDQuadGraphType3.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation3.Items.AddRange(DropDownListNSDQuadGraphLocation3.Items.OfType<ListItem>().ToArray());
                            }
                            if (DropDownListNSDQuadGraphSource4.SelectedIndex > 0)
                            {
                                DropDownListModalCellSource4.Items.AddRange(DropDownListNSDQuadGraphSource4.Items.OfType<ListItem>().ToArray());
                                DropDownListModalDataType4.Items.AddRange(DropDownListNSDQuadGraphType4.Items.OfType<ListItem>().ToArray());
                                DropDownListModalLocation4.Items.AddRange(DropDownListNSDQuadGraphLocation4.Items.OfType<ListItem>().ToArray());
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < PanelModalChart.Controls.Count; i++)
                        PanelModalChart.Controls[i].Visible = false;
                }
            }
            else
            {
                for (int i = 0; i < PanelAllModal.Controls.Count; i++)
                    PanelAllModal.Controls[i].Visible = false;
                for (int i = 0; i < PanelModalChart.Controls.Count; i++)
                    PanelModalChart.Controls[i].Visible = false;
            }

            if (!selectedDropDown.ClientID.Contains("Modal"))
            {

                if ((CDInput.SelectedItem.ToString().Equals("NSD") &&
                (CDLayout.SelectedItem.ToString().Equals("Single") || CDLayout.SelectedItem.ToString().Equals("Wide"))) ||
                (CDInput.SelectedItem.ToString().Equals("NSD") && CDCellType.SelectedItem.ToString().Equals("GraphView") &&
                (CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Wide"))))
                {
                    for (int i = 0; i < PanelNSDSingle.Controls.Count; i++)
                        PanelNSDSingle.Controls[i].Visible = true;

                    using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                    {
                        String SourceQuery = "select distinct NSDSourceID, NSDSource from Cdef.NSDXRef";
                        DropDownListCellSource.Items.Clear();
                        DropDownListDataType.Items.Clear();
                        DropDownListLocation.Items.Clear();
                        BindDropDownList(sql, DropDownListCellSource, SourceQuery, "NSDSource", "NSDSourceID", "Select Source");
                        sql.Close();
                    }
                }
                else
                {
                    for (int i = 0; i < PanelNSDSingle.Controls.Count; i++)
                        PanelNSDSingle.Controls[i].Visible = false;
                }

                if (CDInput.SelectedItem.ToString().Equals("NSD") && 
                    (CDLayout.SelectedItem.ToString().Equals("Wide") || CDLayout.SelectedItem.ToString().Equals("Quad")) &&
                    (!CDCellType.SelectedItem.ToString().Equals("Select") && !CDCellType.SelectedItem.ToString().Equals("GraphView")))
                {
                    for (int i = 0; i < PanelNSDWide.Controls.Count; i++)
                        PanelNSDWide.Controls[i].Visible = true;
                }
                else
                {
                    for (int i = 0; i < PanelNSDWide.Controls.Count; i++)
                        PanelNSDWide.Controls[i].Visible = false;

                    for (int i = 0; i < PanelNSDTallExclusive.Controls.Count; i++)
                        PanelNSDTallExclusive.Controls[i].Visible = false;
                }

                if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                    (CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Quad")) &&
                    (!CDCellType.SelectedItem.ToString().Equals("GraphView") && !CDCellType.SelectedItem.ToString().Equals("Select")))
                {
                    for (int i = 0; i < PanelNSDTall.Controls.Count; i++)
                        PanelNSDTall.Controls[i].Visible = true;

                    if (CDLayout.SelectedItem.ToString().Equals("Tall"))
                    {
                        for (int i = 0; i < PanelNSDTallExclusive.Controls.Count; i++)
                            PanelNSDTallExclusive.Controls[i].Visible = true;
                    }
                    else
                    {
                        for (int i = 0; i < PanelNSDTallExclusive.Controls.Count; i++)
                            PanelNSDTallExclusive.Controls[i].Visible = false;
                    }

                    using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                    {
                        String SourceQuery = "select distinct NSDSourceID, NSDSource from Cdef.NSDXRef";
                        DropDownListNSDTallSource1.Items.Clear();
                        DropDownListNSDTallType1.Items.Clear();
                        DropDownListNSDTallLocation1.Items.Clear();
                        BindDropDownList(sql, DropDownListNSDTallSource1, SourceQuery, "NSDSource", "NSDSourceID", "Select Source");
                        DropDownListNSDTallSource2.Items.Clear();
                        DropDownListNSDTallSource3.Items.Clear();
                        DropDownListNSDTallSource4.Items.Clear();
                        DropDownListNSDTallSource2.Items.AddRange(DropDownListNSDTallSource1.Items.OfType<ListItem>().ToArray());
                        DropDownListNSDTallSource3.Items.AddRange(DropDownListNSDTallSource1.Items.OfType<ListItem>().ToArray());
                        DropDownListNSDTallSource4.Items.AddRange(DropDownListNSDTallSource1.Items.OfType<ListItem>().ToArray());
                        sql.Close();
                    }
                }
                else if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                    CDLayout.SelectedItem.ToString().Equals("Tall") &&
                    CDCellType.SelectedItem.ToString().Equals("GraphView"))
                {

                    for (int i = 0; i < PanelNSDTallExclusive.Controls.Count; i++)
                        PanelNSDTallExclusive.Controls[i].Visible = true;
                }
                else
                {
                    for (int i = 0; i < PanelNSDTall.Controls.Count; i++)
                        PanelNSDTall.Controls[i].Visible = false;
                    for (int i = 0; i < PanelNSDTallExclusive.Controls.Count; i++)
                        PanelNSDTallExclusive.Controls[i].Visible = false;
                }

                if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Quad") && CDCellType.SelectedItem.ToString().Equals("GraphView"))
                {
                    for (int i = 0; i < PanelNSDQuadGraph.Controls.Count; i++)
                        PanelNSDQuadGraph.Controls[i].Visible = true;

                    using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                    {
                        String SourceQuery = "select distinct NSDSourceID, NSDSource from Cdef.NSDXRef";
                        DropDownListNSDQuadGraphSource1.Items.Clear();
                        DropDownListNSDQuadGraphType1.Items.Clear();
                        DropDownListNSDQuadGraphLocation1.Items.Clear();
                        BindDropDownList(sql, DropDownListNSDQuadGraphSource1, SourceQuery, "NSDSource", "NSDSourceID", "Select Source");
                        DropDownListQuadGraphCDLineColor1.Items.Clear();
                        DropDownListQuadGraphCDLineColor2.Items.Clear();
                        DropDownListQuadGraphCDLineColor3.Items.Clear();
                        DropDownListQuadGraphCDLineColor4.Items.Clear();
                        listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListQuadGraphCDLineColor1);
                        listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListQuadGraphCDLineColor2);
                        listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListQuadGraphCDLineColor3);
                        listSourceBind(sql, "[Cdef].[CellColor]", "CDColorID", "CellColor", DropDownListQuadGraphCDLineColor4);
                        
                        DropDownListNSDQuadGraphSource2.Items.Clear();
                        DropDownListNSDQuadGraphSource3.Items.Clear();
                        DropDownListNSDQuadGraphSource4.Items.Clear();
                        DropDownListNSDQuadGraphSource2.Items.AddRange(DropDownListNSDQuadGraphSource1.Items.OfType<ListItem>().ToArray());
                        DropDownListNSDQuadGraphSource3.Items.AddRange(DropDownListNSDQuadGraphSource1.Items.OfType<ListItem>().ToArray());
                        DropDownListNSDQuadGraphSource4.Items.AddRange(DropDownListNSDQuadGraphSource1.Items.OfType<ListItem>().ToArray());
                        sql.Close();
                    }
                }
                else
                {
                    for (int i = 0; i < PanelNSDQuadGraph.Controls.Count; i++)
                        PanelNSDQuadGraph.Controls[i].Visible = false;
                }



                if (CDCellType.SelectedItem.ToString().Equals("GraphView") && !CDLayout.SelectedItem.ToString().Equals("Quad"))
                {
                    for (int i = 0; i < PanelGraphView.Controls.Count; i++)
                        PanelGraphView.Controls[i].Visible = true;
                }
                else
                {
                    for (int i = 0; i < PanelGraphView.Controls.Count; i++)
                        PanelGraphView.Controls[i].Visible = false;
                }
            }
            
        }


        protected void CDInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPanelVisibility(sender);
        }
        protected void CDLayout_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPanelVisibility(sender);
        }
        protected void ModalContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPanelVisibility(sender);
        }
        protected void ModalContentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPanelVisibility(sender);
        }
   
        protected int SaveCellDefinition()
        {
            SqlCommand CellSourceInsertSql = null;
            int CellDefinitionId = -1;

            using (SqlConnection conn = new SqlConnection(Constants.CDManagerDB))
            {
                SqlCommand CellDefInsertSql = new SqlCommand(
                    "insert into Cdef.CellDefinition(CellStatus, Title, LongTitle,CellDescription,CDInputID,CDLayoutID,CDCellTypeId,XRefId,IsAd,CDAdLevelID,CDColorID,ExpirationDate,ImagePath,ImageOverlay,OverlayColor,OverlayPosition,Story,StoryStyle,URLPath,PostStatus,Tags,IsModal,LastUpdateUser,LastUpdateTime)"
                    + "values(@CellStatus, @Title, @LongTitle, @CellDescription,@CDInputID,@CDLayoutID,@CDCellTypeId,@XRefId,@IsAd,@CDAdLevelID,@CDColorID,@ExpirationDate,@ImagePath,@ImageOverlay,@OverlayColor,@OverlayPosition,@Story,@StoryStyle,@URLPath,@PostStatus,@Tags,@IsModal,@LastUpdateUser,@LastUpdateTime);"
                    + "SELECT CAST(scope_identity() AS int)",
                    conn);
                CellDefInsertSql.Parameters.AddWithValue("@CellStatus", Convert.ToBoolean(Convert.ToInt16(Status.SelectedValue)));
                CellDefInsertSql.Parameters.AddWithValue("@Title", Title.Text);
                CellDefInsertSql.Parameters.AddWithValue("@LongTitle", LongTitle.Text);
                CellDefInsertSql.Parameters.AddWithValue("@CellDescription", CellDescription.Text);
                CellDefInsertSql.Parameters.AddWithValue("@CDInputID", CDInput.SelectedValue);
                CellDefInsertSql.Parameters.AddWithValue("@CDLayoutID", CDLayout.SelectedValue);
                CellDefInsertSql.Parameters.AddWithValue("@CDCellTypeId", CDCellType.SelectedValue);
                if (CDInput.SelectedItem.ToString().Equals("NSD") &&
                    (CDLayout.SelectedItem.ToString().Equals("Single") || CDLayout.SelectedItem.ToString().Equals("Wide"))) {
                    CellDefInsertSql.Parameters.AddWithValue("@XRefId", Convert.ToInt32(XRefId.Value));
                }
                else {
                    CellDefInsertSql.Parameters.AddWithValue("@XRefId", 0);
                }
                CellDefInsertSql.Parameters.AddWithValue("@IsAd", Convert.ToBoolean(Convert.ToInt16(Ad.SelectedValue)));
                CellDefInsertSql.Parameters.AddWithValue("@CDAdLevelID", DBNull.Value);
                if (!CDColor.SelectedItem.ToString().Equals("Select"))
                    CellDefInsertSql.Parameters.AddWithValue("@CDColorID", CDColor.SelectedValue);
                else
                    CellDefInsertSql.Parameters.AddWithValue("@CDColorID", DBNull.Value);
                if (!String.IsNullOrEmpty(ExpirationDate.Text))
                    CellDefInsertSql.Parameters.AddWithValue("@ExpirationDate", ExpirationDate.Text);
                else
                    CellDefInsertSql.Parameters.AddWithValue("@ExpirationDate", DBNull.Value);
                CellDefInsertSql.Parameters.AddWithValue("@ImagePath", ImagePath.Text);
                if (CDCellType.SelectedItem.Text.Equals("Image W Overlay")) {
                    CellDefInsertSql.Parameters.AddWithValue("@ImageOverlay", ImageOverlay.Text);
                    CellDefInsertSql.Parameters.AddWithValue("@OverlayColor", DropDownListOverlayColor.SelectedValue);
                    CellDefInsertSql.Parameters.AddWithValue("@OverlayPosition", DropDownListOverlayPosition.SelectedValue);
                }
                else
                {
                    CellDefInsertSql.Parameters.AddWithValue("@ImageOverlay", DBNull.Value);
                    CellDefInsertSql.Parameters.AddWithValue("@OverlayColor", DBNull.Value);
                    CellDefInsertSql.Parameters.AddWithValue("@OverlayPosition", DBNull.Value);
                }
                CellDefInsertSql.Parameters.AddWithValue("@Story", Story.Text);
                CellDefInsertSql.Parameters.AddWithValue("@StoryStyle", DBNull.Value);
                CellDefInsertSql.Parameters.AddWithValue("@URLPath", URLPath.Text);
                CellDefInsertSql.Parameters.AddWithValue("@PostStatus", Boolean.FalseString);
                CellDefInsertSql.Parameters.AddWithValue("@Tags", Tags.Text);
                CellDefInsertSql.Parameters.AddWithValue("@IsModal", Convert.ToBoolean(Convert.ToInt16(DropDownListModalContent.SelectedValue)));
                CellDefInsertSql.Parameters.AddWithValue("@LastUpdateUser", Session["UserName"]);
                CellDefInsertSql.Parameters.AddWithValue("@LastUpdateTime", DateTime.Now);

                try
                {
                    conn.Open();
                    CellDefinitionId = (Int32)CellDefInsertSql.ExecuteScalar();
                    CellId.Text = CellDefinitionId.ToString();
                    if ((CellDefinitionId > 0) && (CDInput.SelectedItem.ToString().Equals("NSD")))
                    {
                        int CellInputId;

                        if (CDLayout.SelectedItem.ToString().Equals("Single") || CDLayout.SelectedItem.ToString().Equals("Wide"))
                        {
                            CellSourceInsertSql = new SqlCommand(
                        "insert into Cdef.CellSource(CellDefinitionId, InputID, CellSource,CellDataType,CellLocation,Period1,Period2,Period3,Heading1,Heading2,Heading3,NumberOfEvents,XaxisLabel,YaxisLabel,Legend,LineColor)"
                        + "values(@CellDefinitionId, @InputID, @CellSource,@CellDataType,@CellLocation,@Period1,@Period2,@Period3,@Heading1,@Heading2,@Heading3,@NumberOfEvents,@XaxisLabel,@YaxisLabel,@Legend,@LineColor)",
                        conn);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellDefinitionId", CellDefinitionId);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListCellSource.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListDataType.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListLocation.SelectedValue);
                        }

                        if (CDLayout.SelectedItem.ToString().Equals("Single"))
                        {
                            CellInputId = 1;
                            CellSourceInsertSql.Parameters.AddWithValue("@InputID", CellInputId);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period1", "Current");
                            CellSourceInsertSql.Parameters.AddWithValue("@Period2", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period3", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading1", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading2", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading3", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", 1);
                            CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Legend", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DBNull.Value);
                            CellSourceInsertSql.ExecuteNonQuery();
                        }
                        else if (CDLayout.SelectedItem.ToString().Equals("Wide"))
                        {
                            CellInputId = 1;
                            CellSourceInsertSql.Parameters.AddWithValue("@InputID", CellInputId);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period1", DropDownListPeriod1.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period2", DropDownListPeriod2.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period3", DropDownListPeriod3.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading1", TextBoxHeading1.Text);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading2", TextBoxHeading2.Text);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading3", TextBoxHeading3.Text);
                            if (!CDCellType.SelectedItem.ToString().Equals("GraphView"))
                            {
                                CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", 3);
                                CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@Legend", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DBNull.Value);
                            }
                            else
                            {
                                CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", Convert.ToInt32(NumOfEvents.Text));
                                CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxXAxis.Text);
                                CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", TextBoxYAxis.Text);
                                CellSourceInsertSql.Parameters.AddWithValue("@Legend", TextBoxLegend.Text);
                                CellSourceInsertSql.Parameters.AddWithValue("@LineColor", CDLineColor.SelectedValue);
                            }
                            CellSourceInsertSql.ExecuteNonQuery();
                        }
                        else if ((CDLayout.SelectedItem.ToString().Equals("Tall") || CDLayout.SelectedItem.ToString().Equals("Quad")) &&
                            !CDCellType.SelectedItem.ToString().Equals("GraphView"))
                        {
                            int TallSourceCount = 1;
                            if (DropDownListNSDTallSource1.SelectedIndex > 0) TallSourceCount = 1;
                            if (DropDownListNSDTallSource2.SelectedIndex > 0) TallSourceCount = 2;
                            if (DropDownListNSDTallSource3.SelectedIndex > 0) TallSourceCount = 3;
                            if (DropDownListNSDTallSource4.SelectedIndex > 0) TallSourceCount = 4;

                            for (int i = 1; i <= TallSourceCount; i++)
                            {
                                CellSourceInsertSql = new SqlCommand(
                            "insert into Cdef.CellSource(CellDefinitionId, InputID, CellSource,CellDataType,CellLocation,Period1,Period2,Period3,Heading1,Heading2,Heading3,NumberOfEvents,XaxisLabel,YaxisLabel,Legend,LineColor)"
                            + "values(@CellDefinitionId, @InputID, @CellSource,@CellDataType,@CellLocation,@Period1,@Period2,@Period3,@Heading1,@Heading2,@Heading3,@NumberOfEvents,@XaxisLabel,@YaxisLabel,@Legend,@LineColor)",
                            conn);
                                CellSourceInsertSql.Parameters.AddWithValue("@CellDefinitionId", CellDefinitionId);

                                if (i == 1)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDTallSource1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDTallType1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDTallLocation1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxXAxisLabel1.Text);
                                }
                                else if (i == 2)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDTallSource2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDTallType2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDTallLocation2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxXAxisLabel2.Text);
                                }
                                else if (i == 3)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDTallSource3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDTallType3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDTallLocation3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxXAxisLabel3.Text);
                                }
                                else if (i == 4)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDTallSource4.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDTallType4.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDTallLocation4.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxXAxisLabel4.Text);
                                }

                                CellInputId = i;
                                CellSourceInsertSql.Parameters.AddWithValue("@InputID", CellInputId);
                                if (CDLayout.SelectedItem.ToString().Equals("Tall"))
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@Period1", DropDownListNSDTallPeriod.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Period2", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Period3", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Heading1", NSDTallHeading.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Heading2", TextBoxHeading2.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Heading3", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", 1);
                                    CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Legend", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DBNull.Value);
                                }
                                else if (CDLayout.SelectedItem.ToString().Equals("Quad"))
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@Period1", DropDownListPeriod1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Period2", DropDownListPeriod2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Period3", DropDownListPeriod3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Heading1", TextBoxHeading1.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Heading2", TextBoxHeading2.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Heading3", TextBoxHeading3.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", 1);
                                    CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@Legend", DBNull.Value);
                                    CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DBNull.Value);
                                }
                                CellSourceInsertSql.ExecuteNonQuery();
                            }
                        }
                        else if (CDCellType.SelectedItem.ToString().Equals("GraphView") && !CDLayout.SelectedItem.ToString().Equals("Quad"))
                        {
                            CellSourceInsertSql = new SqlCommand(
                            "insert into Cdef.CellSource(CellDefinitionId, InputID, CellSource,CellDataType,CellLocation,Period1,Period2,Period3,Heading1,Heading2,Heading3,NumberOfEvents,XaxisLabel,YaxisLabel,Legend,LineColor)"
                            + "values(@CellDefinitionId, @InputID, @CellSource,@CellDataType,@CellLocation,@Period1,@Period2,@Period3,@Heading1,@Heading2,@Heading3,@NumberOfEvents,@XaxisLabel,@YaxisLabel,@Legend,@LineColor)",
                            conn);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellDefinitionId", CellDefinitionId);
                            CellSourceInsertSql.Parameters.AddWithValue("@InputID", 1);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListCellSource.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListDataType.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListLocation.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period1", DropDownListNSDTallPeriod.SelectedValue);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period2", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Period3", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading1", NSDTallHeading.Text);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading2", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@Heading3", DBNull.Value);
                            CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", Convert.ToInt32(NumOfEvents.Text));
                            CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxXAxis.Text);
                            CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", TextBoxYAxis.Text);
                            CellSourceInsertSql.Parameters.AddWithValue("@Legend", TextBoxLegend.Text);
                            CellSourceInsertSql.Parameters.AddWithValue("@LineColor", CDLineColor.SelectedValue);

                            CellSourceInsertSql.ExecuteNonQuery();
                        }
                        else if (CDCellType.SelectedItem.ToString().Equals("GraphView") && CDLayout.SelectedItem.ToString().Equals("Quad"))
                        {
                            int NSDQuadGraphSourceCount = 1;
                            if (DropDownListNSDQuadGraphSource1.SelectedIndex > 0) NSDQuadGraphSourceCount = 1;
                            if (DropDownListNSDQuadGraphSource2.SelectedIndex > 0) NSDQuadGraphSourceCount = 2;
                            if (DropDownListNSDQuadGraphSource3.SelectedIndex > 0) NSDQuadGraphSourceCount = 3;
                            if (DropDownListNSDQuadGraphSource4.SelectedIndex > 0) NSDQuadGraphSourceCount = 4;

                            for (int i = 1; i <= NSDQuadGraphSourceCount; i++)
                            {
                                CellSourceInsertSql = new SqlCommand(
                            "insert into Cdef.CellSource(CellDefinitionId, InputID, CellSource,CellDataType,CellLocation,Period1,Period2,Period3,Heading1,Heading2,Heading3,NumberOfEvents,XaxisLabel,YaxisLabel,Legend,LineColor)"
                            + "values(@CellDefinitionId, @InputID, @CellSource,@CellDataType,@CellLocation,@Period1,@Period2,@Period3,@Heading1,@Heading2,@Heading3,@NumberOfEvents,@XaxisLabel,@YaxisLabel,@Legend,@LineColor)",
                            conn);
                                CellSourceInsertSql.Parameters.AddWithValue("@CellDefinitionId", CellDefinitionId);

                                if (i == 1)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDQuadGraphSource1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDQuadGraphType1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDQuadGraphLocation1.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxQuadGraphXAxisLabel1.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DropDownListQuadGraphCDLineColor1.SelectedValue);
                                }
                                else if (i == 2)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDQuadGraphSource2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDQuadGraphType2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDQuadGraphLocation2.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxQuadGraphXAxisLabel2.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DropDownListQuadGraphCDLineColor2.SelectedValue);
                                }
                                else if (i == 3)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDQuadGraphSource3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDQuadGraphType3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDQuadGraphLocation3.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxQuadGraphXAxisLabel3.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DropDownListQuadGraphCDLineColor3.SelectedValue);
                                }
                                else if (i == 4)
                                {
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellSource", DropDownListNSDQuadGraphSource4.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellDataType", DropDownListNSDQuadGraphType4.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@CellLocation", DropDownListNSDQuadGraphLocation4.SelectedValue);
                                    CellSourceInsertSql.Parameters.AddWithValue("@XaxisLabel", TextBoxQuadGraphXAxisLabel4.Text);
                                    CellSourceInsertSql.Parameters.AddWithValue("@LineColor", DropDownListQuadGraphCDLineColor4.SelectedValue);
                                }

                                CellInputId = i;
                                CellSourceInsertSql.Parameters.AddWithValue("@InputID", CellInputId);
                                CellSourceInsertSql.Parameters.AddWithValue("@Period1", DropDownListNSDQuadGraphPeriod.SelectedValue);
                                CellSourceInsertSql.Parameters.AddWithValue("@Period2", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@Period3", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@Heading1", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@Heading2", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@Heading3", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@NumberOfEvents", Convert.ToInt32(QuadGraphNumOfEvents.Text));
                                CellSourceInsertSql.Parameters.AddWithValue("@YaxisLabel", DBNull.Value);
                                CellSourceInsertSql.Parameters.AddWithValue("@Legend", DBNull.Value);
                                CellSourceInsertSql.ExecuteNonQuery();
                            }
                        }
                    }

                    if ((CellDefinitionId > 0) && DropDownListModalContent.SelectedValue.Equals("1"))
                    {
                        /* This is for modal cell definition
                         * step 1: save data to ModalContent table
                         */
                        SqlCommand ModalContentInsertSql = new SqlCommand(
                           "insert into Cdef.ModalContent(CellDefinitionId, ModalLongTitle, ModalDetailNotes, ModalInputID, ModalTypeID, ModalTextOverlay, ModalContentType, ModalGraphTypeID, ModalImageNewsPath, ModalCRIndicator)"
                           + "values(@CellDefinitionId, @ModalLongTitle, @ModalDetailNotes, @ModalInputID, @ModalTypeID, @ModalTextOverlay, @ModalContentType, @ModalGraphTypeID, @ModalImageNewsPath, @ModalCRIndicator)",
                           conn);
                   
                        ModalContentInsertSql.Parameters.AddWithValue("@CellDefinitionId", CellDefinitionId);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalLongTitle", TextBoxModalLongTitle.Text);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalDetailNotes", TextBoxModalDetailNotes.Text);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalInputID", DropDownListModalInput.SelectedValue);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalTypeID", DropDownListModalType.SelectedValue);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalTextOverlay", TextBoxModalTextOverlay.Text);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalContentType", DropDownListModalContentType.SelectedValue);
                        if (!DropDownListModalGraphType.SelectedItem.ToString().Equals("Select"))
                            ModalContentInsertSql.Parameters.AddWithValue("@ModalGraphTypeID", DropDownListModalGraphType.SelectedValue);
                        else
                            ModalContentInsertSql.Parameters.AddWithValue("@ModalGraphTypeID", DBNull.Value);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalImageNewsPath", TextBoxModalImageNewsPath.Text);
                        ModalContentInsertSql.Parameters.AddWithValue("@ModalCRIndicator", Convert.ToBoolean(Convert.ToInt16(DropDownListModalCR.SelectedValue)));
                        ModalContentInsertSql.ExecuteNonQuery();

                        if (DropDownListModalContentType.SelectedValue.ToString().Equals("Graph"))
                        {
                            int ModalGraphSourceCount = 0;
                            if (!String.IsNullOrEmpty(TextBoxModalNumOfEvents1.Text.ToString())) ModalGraphSourceCount = 1;
                            if (!String.IsNullOrEmpty(TextBoxModalNumOfEvents2.Text.ToString())) ModalGraphSourceCount = 2;
                            if (!String.IsNullOrEmpty(TextBoxModalNumOfEvents3.Text.ToString())) ModalGraphSourceCount = 3;
                            if (!String.IsNullOrEmpty(TextBoxModalNumOfEvents4.Text.ToString())) ModalGraphSourceCount = 4;

                            
                            for (int i=1; i<= ModalGraphSourceCount; i++)
                            {
                                SqlCommand ModalSourceInsertSql = new SqlCommand(
                                "insert into Cdef.ModalSources(CellDefinitionId,ModalSourceCounter,ModalCellSource,ModalCellDataType,ModalCellLocation,ModalNumOfEvents,ModalPeriod,ModalLineColor,ModalLabel)"
                                + "values(@CellDefinitionId, @ModalSourceCounter, @ModalCellSource, @ModalCellDataType, @ModalCellLocation, @ModalNumOfEvents, @ModalPeriod, @ModalLineColor, @ModalLabel)",
                                conn);

                                ModalSourceInsertSql.Parameters.AddWithValue("@CellDefinitionId", CellDefinitionId);
                                ModalSourceInsertSql.Parameters.AddWithValue("@ModalSourceCounter", i);
                                if (i == 1)
                                {
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellSource", DropDownListModalCellSource1.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellDataType", DropDownListModalDataType1.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellLocation", DropDownListModalLocation1.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalNumOfEvents", Convert.ToInt32(TextBoxModalNumOfEvents1.Text));
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalPeriod", DropDownListModalPeriod1.SelectedValue);
                                    if (!DropDownListModalLineColor1.SelectedValue.Equals("Select"))
                                        ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DropDownListModalLineColor1.SelectedValue);
                                    else ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DBNull.Value);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalLabel", TextBoxModalLabel1.Text);
                                }
                                else if (i == 2)
                                {
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellSource", DropDownListModalCellSource2.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellDataType", DropDownListModalDataType2.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellLocation", DropDownListModalLocation2.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalNumOfEvents", Convert.ToInt32(TextBoxModalNumOfEvents2.Text));
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalPeriod", DropDownListModalPeriod2.SelectedValue);
                                    if (!DropDownListModalLineColor2.SelectedValue.Equals("Select")) 
                                        ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DropDownListModalLineColor2.SelectedValue);
                                    else ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DBNull.Value);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalLabel", TextBoxModalLabel2.Text);
                                }
                                else if (i == 3)
                                {
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellSource", DropDownListModalCellSource3.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellDataType", DropDownListModalDataType3.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellLocation", DropDownListModalLocation3.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalNumOfEvents", Convert.ToInt32(TextBoxModalNumOfEvents3.Text));
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalPeriod", DropDownListModalPeriod3.SelectedValue);
                                    if (!DropDownListModalLineColor3.SelectedValue.Equals("Select"))
                                        ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DropDownListModalLineColor3.SelectedValue);
                                    else ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DBNull.Value);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalLabel", TextBoxModalLabel3.Text);
                                }
                                else if (i == 4)
                                {
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellSource", DropDownListModalCellSource4.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellDataType", DropDownListModalDataType4.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalCellLocation", DropDownListModalLocation4.SelectedValue);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalNumOfEvents", Convert.ToInt32(TextBoxModalNumOfEvents4.Text));
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalPeriod", DropDownListModalPeriod4.SelectedValue);
                                    if (!DropDownListModalLineColor4.SelectedValue.Equals("Select"))
                                        ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DropDownListModalLineColor4.SelectedValue);
                                    else ModalSourceInsertSql.Parameters.AddWithValue("@ModalLineColor", DBNull.Value);
                                    ModalSourceInsertSql.Parameters.AddWithValue("@ModalLabel", TextBoxModalLabel4.Text);
                                }
                                ModalSourceInsertSql.ExecuteNonQuery();
                            }
                        }
                    }
                    return CellDefinitionId;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    ExceptionUtility.LogException(ex, "SaveCellDefinition");
                    conn.Close();
                    return CellDefinitionId;
                }
            }
        }

 
        protected void Save_Click(object sender, EventArgs e)
        {
            int CellDefinitionId = -1;
            CellDefinitionId = SaveCellDefinition();
            if (CellDefinitionId>0)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Cell Definition Inserted", "alert('Cell Definition Created')", true);
            else
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Failed to insert cell Definition", "alert('Failed to insert cell Definition')", true);
        }

        protected void SaveRefresh_Click(object sender, EventArgs e)
        {
            int CellDefinitionId = -1, clusterCellId = -1;
            CellDefinitionId = SaveCellDefinition();
            if (CellDefinitionId > 0)
            {
                if (CDCellType.SelectedItem.ToString().Equals("GraphView") && CDLayout.SelectedItem.ToString().Equals("Wide"))
                {
                    clusterCellId = Utilities.RefreshWideGraphCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.Text, DropDownListCellSource.SelectedValue, DropDownListDataType.SelectedValue, DropDownListLocation.SelectedValue, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text, NumOfEvents.Text, CDLineColor.SelectedItem.Text, TextBoxLegend.Text, TextBoxHeading3.Text);                    
                }
                else if (CDCellType.SelectedItem.ToString().Equals("GraphView") && CDLayout.SelectedItem.ToString().Equals("Tall"))
                {
                    clusterCellId = Utilities.RefreshTallGraphCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.Text, DropDownListCellSource.SelectedValue, DropDownListDataType.SelectedValue, DropDownListLocation.SelectedValue, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text, NumOfEvents.Text, CDLineColor.SelectedItem.Text, TextBoxLegend.Text, TextBoxYAxis.Text, NSDTallHeading.Text);
                }
                else if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Single"))
                {
                    clusterCellId = Utilities.RefreshNSDSingleCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, DropDownListCellSource.SelectedValue, DropDownListDataType.SelectedValue, DropDownListLocation.SelectedValue, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text);
                }
                else if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Wide"))
                {
                     clusterCellId = Utilities.RefreshNSDWideCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.Text, DropDownListCellSource.SelectedValue, DropDownListDataType.SelectedValue, DropDownListLocation.SelectedValue, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text, DropDownListPeriod1.SelectedValue, DropDownListPeriod2.SelectedValue, DropDownListPeriod3.SelectedValue, TextBoxHeading2.Text, TextBoxHeading3.Text);
                }
                else if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Tall"))
                {
                    string[] CellSource = { DropDownListNSDTallSource1.SelectedValue, DropDownListNSDTallSource2.SelectedValue, DropDownListNSDTallSource3.SelectedValue, DropDownListNSDTallSource4.SelectedValue };
                    string[] CellDataType = { DropDownListNSDTallType1.SelectedValue, DropDownListNSDTallType2.SelectedValue, DropDownListNSDTallType3.SelectedValue, DropDownListNSDTallType4.SelectedValue};
                    string[] CellLocation = { DropDownListNSDTallLocation1.SelectedValue , DropDownListNSDTallLocation2.SelectedValue , DropDownListNSDTallLocation3.SelectedValue , DropDownListNSDTallLocation4.SelectedValue };
                    string[] xAxisLabel = { TextBoxXAxisLabel1.Text, TextBoxXAxisLabel2.Text, TextBoxXAxisLabel3.Text, TextBoxXAxisLabel4.Text };
                    clusterCellId = Utilities.RefreshNSDTallCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.ToString(), CellSource, CellDataType, CellLocation, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text, NSDTallHeading.Text, DropDownListNSDTallPeriod.SelectedValue, xAxisLabel);
                }
                else if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Quad") && CDCellType.SelectedItem.ToString().Equals("GraphView"))
                {
                    string[] CellSource = { DropDownListNSDQuadGraphSource1.SelectedValue, DropDownListNSDQuadGraphSource2.SelectedValue, DropDownListNSDQuadGraphSource3.SelectedValue, DropDownListNSDQuadGraphSource4.SelectedValue };
                    string[] CellDataType = { DropDownListNSDQuadGraphType1.SelectedValue, DropDownListNSDQuadGraphType2.SelectedValue, DropDownListNSDQuadGraphType3.SelectedValue, DropDownListNSDQuadGraphType4.SelectedValue };
                    string[] CellLocation = { DropDownListNSDQuadGraphLocation1.SelectedValue, DropDownListNSDQuadGraphLocation2.SelectedValue, DropDownListNSDQuadGraphLocation3.SelectedValue, DropDownListNSDQuadGraphLocation4.SelectedValue };
                    string[] xAxisLabel = { TextBoxQuadGraphXAxisLabel1.Text, TextBoxQuadGraphXAxisLabel2.Text, TextBoxQuadGraphXAxisLabel3.Text, TextBoxQuadGraphXAxisLabel4.Text };
                    string[] LineColor = { DropDownListQuadGraphCDLineColor1.SelectedItem.Text, DropDownListQuadGraphCDLineColor2.SelectedItem.Text , DropDownListQuadGraphCDLineColor3.SelectedItem.Text , DropDownListQuadGraphCDLineColor4.SelectedItem.Text};
                    clusterCellId = Utilities.RefreshNSDQuadGraphViewCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.ToString(), CellSource, CellDataType, CellLocation, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text, xAxisLabel, LineColor, DropDownListNSDQuadGraphPeriod.SelectedValue, QuadGraphNumOfEvents.Text);
                }
                else if (CDInput.SelectedItem.ToString().Equals("NSD") && CDLayout.SelectedItem.ToString().Equals("Quad") && !CDCellType.SelectedItem.ToString().Equals("GraphView"))
                {
                    string[] CellSource = { DropDownListNSDTallSource1.SelectedValue, DropDownListNSDTallSource2.SelectedValue, DropDownListNSDTallSource3.SelectedValue, DropDownListNSDTallSource4.SelectedValue };
                    string[] CellDataType = { DropDownListNSDTallType1.SelectedValue, DropDownListNSDTallType2.SelectedValue, DropDownListNSDTallType3.SelectedValue, DropDownListNSDTallType4.SelectedValue };
                    string[] CellLocation = { DropDownListNSDTallLocation1.SelectedValue, DropDownListNSDTallLocation2.SelectedValue, DropDownListNSDTallLocation3.SelectedValue, DropDownListNSDTallLocation4.SelectedValue };
                    string[] xAxisLabel = { TextBoxXAxisLabel1.Text, TextBoxXAxisLabel2.Text, TextBoxXAxisLabel3.Text, TextBoxXAxisLabel4.Text };
                    string[] heading = { TextBoxHeading1.Text, TextBoxHeading2.Text, TextBoxHeading3.Text };
                    string[] period = { DropDownListPeriod1.SelectedValue, DropDownListPeriod2.SelectedValue, DropDownListPeriod3.SelectedValue };
                    clusterCellId = Utilities.RefreshNSDQuadNonGraphViewCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.ToString(), CellSource, CellDataType, CellLocation, CDCellType.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), Tags.Text, heading, period, xAxisLabel);
                }
                else if (CDInput.SelectedItem.ToString().Equals("Other"))
                {
                    clusterCellId = Utilities.RefreshOtherCellDefinition(Session["ResponseToken"].ToString(), CellDefinitionId, clusterCellId, Title.Text, CDLayout.SelectedItem.ToString(), CDInput.SelectedItem.ToString(), CDColor.SelectedItem.ToString(), CDCellType.SelectedItem.ToString(), Tags.Text, ImagePath.Text, ImageOverlay.Text, DropDownListOverlayColor.SelectedValue, DropDownListOverlayColor.SelectedItem.Text, DropDownListOverlayPosition.SelectedValue, DropDownListOverlayPosition.SelectedItem.Text);
                }

                int updatedModalCellId=0;
                if (DropDownListModalContent.SelectedItem.ToString().Equals("Yes") && clusterCellId>0)
                {
                    string[] ModalCellSource = new string[0];
                    string[] ModalDataType = new string[0];
                    string[] ModalLocation = new string[0];
                    string[] ModalPeriod = new string[0];
                    string[] ModalNumOfEvents = new string[0];
                    string[] ModalLineColor = new string[0];
                    string[] ModalLabel = new string[0];



                    if (DropDownListModalContentType.SelectedValue.ToString().Equals("Graph"))
                    {
                        ModalCellSource = new string[]{ DropDownListModalCellSource1.SelectedValue, DropDownListModalCellSource2.SelectedValue, DropDownListModalCellSource3.SelectedValue, DropDownListModalCellSource4.SelectedValue };
                        ModalDataType = new string[] { DropDownListModalDataType1.SelectedValue, DropDownListModalDataType2.SelectedValue, DropDownListModalDataType3.SelectedValue, DropDownListModalDataType4.SelectedValue };
                        ModalLocation = new string[] { DropDownListModalLocation1.SelectedValue, DropDownListModalLocation2.SelectedValue, DropDownListModalLocation3.SelectedValue, DropDownListModalLocation4.SelectedValue };
                        ModalPeriod = new string[] { DropDownListModalPeriod1.SelectedValue, DropDownListModalPeriod2.SelectedValue, DropDownListModalPeriod3.SelectedValue, DropDownListModalPeriod4.SelectedValue };
                        ModalNumOfEvents = new string[] { TextBoxModalNumOfEvents1.Text, TextBoxModalNumOfEvents2.Text, TextBoxModalNumOfEvents3.Text, TextBoxModalNumOfEvents4.Text };
                        ModalLineColor = new string[] { DropDownListModalLineColor1.SelectedItem.Text, DropDownListModalLineColor2.SelectedItem.Text, DropDownListModalLineColor3.SelectedItem.Text, DropDownListModalLineColor4.SelectedItem.Text };
                        ModalLabel = new string[] { TextBoxModalLabel1.Text, TextBoxModalLabel2.Text, TextBoxModalLabel3.Text, TextBoxModalLabel4.Text};
                    }
                    updatedModalCellId = Utilities.ModalCellPost(Session["ResponseToken"].ToString(), clusterCellId, DropDownListModalContentType.SelectedValue, TextBoxModalLongTitle.Text, TextBoxModalImageNewsPath.Text, ModalCellSource, ModalDataType, ModalLocation, ModalPeriod, ModalNumOfEvents, ModalLineColor, ModalLabel);
                }

                if ((clusterCellId>0) && (updatedModalCellId >= 0))
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Cell Definition Inserted", "alert('Cell Definition Created & Posted Successfully')", true);
                else if (clusterCellId < 0) 
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Cell Definition Inserted; But API Post Failed", "alert('Cell Definition Inserted; But API Post Failed')", true);
                else if ((clusterCellId > 0) && (updatedModalCellId < 0))
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Cell Definition Created & Posted Successfully; But modal update failed", "alert('Cell Definition Created & Posted Successfully; But modal update failed')", true);
            }
            else ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Failed to insert cell Definition", "alert('Failed to insert cell Definition')", true);
        }

 
        protected void CDCellType_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < PanelImagePath.Controls.Count; i++)
            {
                if (CDCellType.SelectedItem.ToString().Equals("Image") || CDCellType.SelectedItem.ToString().Equals("Image W Overlay"))
                    PanelImagePath.Controls[i].Visible = true;
                else
                    PanelImagePath.Controls[i].Visible = false;
            }

            for (int i = 0; i < PanelImageOverlay.Controls.Count; i++)
            {
                if (CDCellType.SelectedItem.ToString().Equals("Image W Overlay"))
                    PanelImageOverlay.Controls[i].Visible = true;
                else
                    PanelImageOverlay.Controls[i].Visible = false;
            }

            for (int i = 0; i < PanelStory.Controls.Count; i++)
            {
                if (CDCellType.SelectedItem.ToString().Equals("Story"))
                    PanelStory.Controls[i].Visible = true;
                else
                    PanelStory.Controls[i].Visible = false;
            }

            for (int i = 0; i < PanelURLPath.Controls.Count; i++)
            {
                if (CDCellType.SelectedItem.ToString().Equals("URL"))
                    PanelURLPath.Controls[i].Visible = true;
                else
                    PanelURLPath.Controls[i].Visible = false;
            }

            setPanelVisibility(sender);
        }

        protected void DropDownListCellSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownListDataType.Enabled = false;
            DropDownListLocation.Enabled = false;
            DropDownListDataType.Items.Clear();
            DropDownListLocation.Items.Clear();
            int cellSourceId = int.Parse(DropDownListCellSource.SelectedItem.Value);
            if (cellSourceId > 0)
            {
                using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                {
                    string SourceQuery = string.Format("select distinct NSDDataTypeID, NSDDataType from Cdef.NSDXRef where NSDSourceID = {0}", cellSourceId);
                    BindDropDownList(sql, DropDownListDataType, SourceQuery, "NSDDataType", "NSDDataTypeID", "Select Data type");
                    sql.Close();
                    DropDownListDataType.Enabled = true;
                }
            }
        }
    
        protected void GenericDropDownListCellSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList NSDTallSource = sender as DropDownList;
            DropDownList GenDropDownListCellSource=null, GenDropDownListDataType=null, GenDropDownListLocation=null;
            if (NSDTallSource.ClientID == "DropDownListNSDTallSource1")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource1;
                GenDropDownListDataType = DropDownListNSDTallType1;
                GenDropDownListLocation = DropDownListNSDTallLocation1;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDTallSource2")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource2;
                GenDropDownListDataType = DropDownListNSDTallType2;
                GenDropDownListLocation = DropDownListNSDTallLocation2;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDTallSource3")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource3;
                GenDropDownListDataType = DropDownListNSDTallType3;
                GenDropDownListLocation = DropDownListNSDTallLocation3;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDTallSource4")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource4;
                GenDropDownListDataType = DropDownListNSDTallType4;
                GenDropDownListLocation = DropDownListNSDTallLocation4;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDQuadGraphSource1")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource1;
                GenDropDownListDataType = DropDownListNSDQuadGraphType1;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation1;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDQuadGraphSource2")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource2;
                GenDropDownListDataType = DropDownListNSDQuadGraphType2;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation2;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDQuadGraphSource3")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource3;
                GenDropDownListDataType = DropDownListNSDQuadGraphType3;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation3;
            }
            else if (NSDTallSource.ClientID == "DropDownListNSDQuadGraphSource4")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource4;
                GenDropDownListDataType = DropDownListNSDQuadGraphType4;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation4;
            }
            else if (NSDTallSource.ClientID == "DropDownListModalCellSource1")
            {
                GenDropDownListCellSource = DropDownListModalCellSource1;
                GenDropDownListDataType = DropDownListModalDataType1;
                GenDropDownListLocation = DropDownListModalLocation1;
            }
            else if (NSDTallSource.ClientID == "DropDownListModalCellSource2")
            {
                GenDropDownListCellSource = DropDownListModalCellSource2;
                GenDropDownListDataType = DropDownListModalDataType2;
                GenDropDownListLocation = DropDownListModalLocation2;
            }
            else if (NSDTallSource.ClientID == "DropDownListModalCellSource3")
            {
                GenDropDownListCellSource = DropDownListModalCellSource3;
                GenDropDownListDataType = DropDownListModalDataType3;
                GenDropDownListLocation = DropDownListModalLocation3;
            }
            else if (NSDTallSource.ClientID == "DropDownListModalCellSource4")
            {
                GenDropDownListCellSource = DropDownListModalCellSource4;
                GenDropDownListDataType = DropDownListModalDataType4;
                GenDropDownListLocation = DropDownListModalLocation4;
            }
            GenDropDownListDataType.Enabled = false;
            GenDropDownListLocation.Enabled = false;
            GenDropDownListDataType.Items.Clear();
            GenDropDownListLocation.Items.Clear();
            int cellSourceId = int.Parse(GenDropDownListCellSource.SelectedItem.Value);
            if (cellSourceId > 0)
            {
                using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                {
                    string SourceQuery = string.Format("select distinct NSDDataTypeID, NSDDataType from Cdef.NSDXRef where NSDSourceID = {0}", cellSourceId);
                    BindDropDownList(sql, GenDropDownListDataType, SourceQuery, "NSDDataType", "NSDDataTypeID", "Select Data type");
                    sql.Close();
                    GenDropDownListDataType.Enabled = true;
                }
            }
        }

        protected void DropDownListDataType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownListLocation.Enabled = false;
            DropDownListLocation.Items.Clear();
            int cellSourceId = int.Parse(DropDownListCellSource.SelectedItem.Value);
            int cellDataType = int.Parse(DropDownListDataType.SelectedItem.Value);
            if ((cellSourceId > 0) && (cellDataType > 0))
            {
                using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                {
                    string SourceQuery = string.Format("select distinct NSDLocationID, NSDLocation from Cdef.NSDXRef where NSDSourceID = {0} and NSDDataTypeID = {1}", cellSourceId, cellDataType);
                    BindDropDownList(sql, DropDownListLocation, SourceQuery, "NSDLocation", "NSDLocationID", "Select Data type");
                    sql.Close();
                    DropDownListLocation.Enabled = true;
                }
            }
        }

        protected void GenericDropDownListDataType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList NSDTallDataType = sender as DropDownList;
            DropDownList GenDropDownListCellSource = null, GenDropDownListDataType = null, GenDropDownListLocation = null;
            if (NSDTallDataType.ClientID == "DropDownListNSDTallType1")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource1;
                GenDropDownListDataType = DropDownListNSDTallType1;
                GenDropDownListLocation = DropDownListNSDTallLocation1;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDTallType2")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource2;
                GenDropDownListDataType = DropDownListNSDTallType2;
                GenDropDownListLocation = DropDownListNSDTallLocation2;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDTallType3")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource3;
                GenDropDownListDataType = DropDownListNSDTallType3;
                GenDropDownListLocation = DropDownListNSDTallLocation3;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDTallType4")
            {
                GenDropDownListCellSource = DropDownListNSDTallSource4;
                GenDropDownListDataType = DropDownListNSDTallType4;
                GenDropDownListLocation = DropDownListNSDTallLocation4;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDQuadGraphType1")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource1;
                GenDropDownListDataType = DropDownListNSDQuadGraphType1;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation1;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDQuadGraphType2")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource2;
                GenDropDownListDataType = DropDownListNSDQuadGraphType2;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation2;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDQuadGraphType3")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource3;
                GenDropDownListDataType = DropDownListNSDQuadGraphType3;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation3;
            }
            else if (NSDTallDataType.ClientID == "DropDownListNSDQuadGraphType4")
            {
                GenDropDownListCellSource = DropDownListNSDQuadGraphSource4;
                GenDropDownListDataType = DropDownListNSDQuadGraphType4;
                GenDropDownListLocation = DropDownListNSDQuadGraphLocation4;
            }
            else if (NSDTallDataType.ClientID == "DropDownListModalDataType1")
            {
                GenDropDownListCellSource = DropDownListModalCellSource1;
                GenDropDownListDataType = DropDownListModalDataType1;
                GenDropDownListLocation = DropDownListModalLocation1;
            }
            else if (NSDTallDataType.ClientID == "DropDownListModalDataType2")
            {
                GenDropDownListCellSource = DropDownListModalCellSource2;
                GenDropDownListDataType = DropDownListModalDataType2;
                GenDropDownListLocation = DropDownListModalLocation2;
            }
            else if (NSDTallDataType.ClientID == "DropDownListModalDataType3")
            {
                GenDropDownListCellSource = DropDownListModalCellSource3;
                GenDropDownListDataType = DropDownListModalDataType3;
                GenDropDownListLocation = DropDownListModalLocation3;
            }
            else if (NSDTallDataType.ClientID == "DropDownListModalDataType4")
            {
                GenDropDownListCellSource = DropDownListModalCellSource4;
                GenDropDownListDataType = DropDownListModalDataType4;
                GenDropDownListLocation = DropDownListModalLocation4;
            }

            GenDropDownListLocation.Enabled = false;
            GenDropDownListLocation.Items.Clear();
            int cellSourceId = int.Parse(GenDropDownListCellSource.SelectedItem.Value);
            int cellDataType = int.Parse(GenDropDownListDataType.SelectedItem.Value);
            if ((cellSourceId > 0) && (cellDataType > 0))
            {
                using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                {
                    string SourceQuery = string.Format("select distinct NSDLocationID, NSDLocation from Cdef.NSDXRef where NSDSourceID = {0} and NSDDataTypeID = {1}", cellSourceId, cellDataType);
                    BindDropDownList(sql, GenDropDownListLocation, SourceQuery, "NSDLocation", "NSDLocationID", "Select Data type");
                    sql.Close();
                    GenDropDownListLocation.Enabled = true;
                }
            }
        }

        protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            string xRefCycle=null;
            int cellSourceId=-1, cellDataType=-1, CellLocation=-1;
            if (CDLayout.SelectedItem.ToString().Equals("Single") || 
                CDLayout.SelectedItem.ToString().Equals("Wide") ||
                (CDLayout.SelectedItem.ToString().Equals("Tall") && CDCellType.SelectedItem.ToString().Equals("GraphView"))) {
                cellSourceId = int.Parse(DropDownListCellSource.SelectedItem.Value);
                cellDataType = int.Parse(DropDownListDataType.SelectedItem.Value);
                CellLocation = int.Parse(DropDownListLocation.SelectedItem.Value);
            }
            else if (CDLayout.SelectedItem.ToString().Equals("Quad") && CDCellType.SelectedItem.ToString().Equals("GraphView"))
            {
                cellSourceId = int.Parse(DropDownListNSDQuadGraphSource1.SelectedItem.Value);
                cellDataType = int.Parse(DropDownListNSDQuadGraphType1.SelectedItem.Value);
                CellLocation = int.Parse(DropDownListNSDQuadGraphLocation1.SelectedItem.Value);
            }
            else
            {
                cellSourceId = int.Parse(DropDownListNSDTallSource1.SelectedItem.Value);
                cellDataType = int.Parse(DropDownListNSDTallType1.SelectedItem.Value);
                CellLocation = int.Parse(DropDownListNSDTallLocation1.SelectedItem.Value);
            }
            if ((cellSourceId > 0) && (cellDataType > 0) && (CellLocation > 0))
            {
                string SourceQuery = string.Format("select XrefID, cycle from Cdef.NSDXRef where NSDSourceID = {0} and NSDDataTypeID = {1} and NSDLocationID = {2}", cellSourceId, cellDataType, CellLocation);
                               
                using (SqlConnection sql = new SqlConnection(Constants.CDManagerDB))
                {
                    SqlCommand cmd = new SqlCommand(SourceQuery, sql);
                    sql.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            XRefId.Value = Convert.ToString(reader.GetInt32(0));
                            xRefCycle = reader.GetString(1);
                        }
                    }
                }

                DropDownListPeriod2.Items.Clear();
                DropDownListPeriod3.Items.Clear();
                
                for (int i = 1; i < 10; i++)
                {
                    if (i == 1)
                    {
                        DropDownListPeriod2.Items.Add(new ListItem(String.Concat(i, " ", xRefCycle), String.Concat(i, " ", xRefCycle)));
                        DropDownListPeriod3.Items.Add(new ListItem("1 Year", "1 Year"));
                        DropDownListPeriod3.Items.Add(new ListItem(String.Concat(i, " ", xRefCycle), String.Concat(i, " ", xRefCycle)));
                    }
                    else
                    {
                        DropDownListPeriod2.Items.Add(new ListItem(String.Concat(i, " ", xRefCycle, "s"), String.Concat(i, " ", xRefCycle, "s")));
                        DropDownListPeriod3.Items.Add(new ListItem(String.Concat(i, " ", xRefCycle, "s"), String.Concat(i, " ", xRefCycle, "s")));
                    }
                }
            }
        }
 
        protected void Clear_Click(object sender, EventArgs e)
        {
             Response.Redirect("~/CellDefinition.aspx");
        }
    }
}