﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDMManager
{
    public class BaseCellDefinition
    {
        public int? Id { get; set; }
        
        public string title { get; set; }

        public string size { get;  set; }

        public string layout { get; set; }

        public string color { get; set; }

        public string[] tags { get; set; }
    }

    public class SingleCell : BaseCellDefinition
    {

        public SingleCell()
        {
            this.size = "single";
        }
    }

    public class SingleNumericCell : SingleCell
    {
        public SingleNumericCellData data { get; set; }

        public SingleNumericCell()
        {
            this.data = new SingleNumericCellData();
        }

    }
    public class SingleNumericCellData
    {
        public double value { get; set; }
        
        public string suffix { get; set; }

        public int? trend { get; set; }

        public string suffixColor { get; set; }

        public SingleNumericCellData() { }
    }

    public class OtherCell:BaseCellDefinition
    {
        public OtherCellData data { get; set; }

        public OtherCell()
        {
            this.data = new OtherCellData();
        }
    }

    public class OtherCellData
    {
        public string imgSrc { get; set; }
        public string captionText { get; set; }
        public CaptionOptions captionOptions { get; set; }
        public OtherCellData() {
            this.captionOptions = new CaptionOptions();
        }
    }

    public class WideNumericCell : BaseCellDefinition
    {
        public WideNumericCellData data { get; set; }

        public WideNumericCell()
        {
            this.data = new WideNumericCellData();
        }
    }

    public class WideNumericCellData
    {
        public string[,] table { get; set; }

        public WideNumericCellData() { }
    }

    public class CaptionOptions
    {
        public string color { get; set; }
        public string location { get; set; }
        public CaptionOptions() { }
    }

    public class TallNumericCell : BaseCellDefinition
    {
        public TallNumericCellData data { get; set; }

        public TallNumericCell()
        {
            this.data = new TallNumericCellData();
        }
    }

    public class TallNumericCellData
    {
        public string[,] table { get; set; }

        public TallNumericCellData() { }
    }

    public class QuadNumericCell : BaseCellDefinition
    {
        public QuadNumericCellData data { get; set; }

        public QuadNumericCell()
        {
            this.data = new QuadNumericCellData();
        }
    }

    public class QuadNumericCellData
    {
        public string[,] table { get; set; }

        public QuadNumericCellData() { }
    }

    public class WideNumericGraphCell: BaseCellDefinition
    {
        public WideNumericGraphCellData data { get; set; }
        public WideNumericGraphCell()
        {
            this.data = new WideNumericGraphCellData();
        }
    }

    public class TallNumericGraphCell : BaseCellDefinition
    {
        public TallNumericGraphCellData data { get; set; }
        public TallNumericGraphCell()
        {
            this.data = new TallNumericGraphCellData();
        }
    }

    public class QuadNumericGraphCell : BaseCellDefinition
    {
        public QuadNumericGraphCellData data { get; set; }
        public QuadNumericGraphCell()
        {
            this.data = new QuadNumericGraphCellData();
        }
    }

    public class GraphCellData
    {
      
        public string[] labels { get; set; }
        public ChartData[] chartData { get; set; }

        public ChartOptions chartOptions { get; set; }

        public GraphCellData() {
            this.chartData = new ChartData[] { new ChartData()};
            this.chartOptions = new ChartOptions();
        }
    }

    public class WideNumericGraphCellData:GraphCellData
    {
        public string[,] captions { get; set; }

        public WideNumericGraphCellData() { }
    }

    public class TallNumericGraphCellData : GraphCellData
    {
        public string caption { get; set; }

        public TallNumericGraphCellData() { }
    }

    public class QuadNumericGraphCellData: GraphCellData
    {
        public QuadNumericGraphCellData() { }
    }

    public class ChartData
    {
        public double[] data { get; set; }
        public string label { get; set; }
        
    }

    public class ChartOptions
    {
        public string[] lineColor { get; set; }

        public string[] axisType { get; set; }

        public ChartOptions() { }
    }

    public class ModalUpdate
    {
        public int? id { get; set; }

        public string longTitle { get; set; }

        public string modalLayout { get; set; }

        public ModalDetail modalDetail { get; set; }

        public ModalUpdate()
        {
            this.modalDetail = new ModalDetail();
        }

    }

    public class ModalDetail
    {
        public string url { get; set; }
        public string notes { get; set; }
        public int? cellId { get; set; }
        public ModalOuterChartData chartData { get; set; }

        public ModalDetail()
        {
        }
    }

    public class ModalOuterChartData
    {
        public string[] label { get; set; }
        public ChartData[] chartData { get; set; }
        public ChartOptions chartOptions { get; set; }

        public ModalOuterChartData()
        {
        }
    }

}