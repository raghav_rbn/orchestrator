﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CDMManager
{
    public static class Constants
    {
        public static string[,] CellSizeAPIMapping =
        {
            {"Single", "single" },
            {"Wide", "wide" },
            {"Tall", "tall" },
            {"Quad", "quad" }
        };

        public static string AuthUrl = "https://cluster-api-endpoint.herokuapp.com/api/1.0/user/auth";
        public static string CellEndPoint = "https://cluster-api-endpoint.herokuapp.com/api/1.0/cell";
        public static string AuthMethod = "POST";
        public static string AuthContentType = "application/json";
        public static string FlowDataDB = ConfigurationManager.ConnectionStrings["RBN_StagingConnectionString"].ConnectionString;
        public static string CDManagerDB = ConfigurationManager.ConnectionStrings["CDM_DevConnectionString"].ConnectionString;
    }
}