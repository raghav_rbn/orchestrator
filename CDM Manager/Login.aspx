﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CDMManager.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="LoginForm" runat="server">
        <br />
        <br />
        <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/CellDefinition.aspx" OnAuthenticate="OnAuthenticate" OnLoginError="Page_Load">
        </asp:Login>
    </form>
</body>
</html>
