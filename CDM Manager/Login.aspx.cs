﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CDMManager
{
    public partial class Login : System.Web.UI.Page
    {
        //private string AuthUrl = "https://cluster-api-endpoint.herokuapp.com/api/1.0/user/auth";
        //private string AuthMethod = "POST";
        //private string AuthContentType = "application/json";
        private string ResponseToken;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
        {
            try
            {
                using (WebClient client = new WebClient())
                {


                    client.Headers["Content-Type"] = Constants.AuthContentType;
                    String authString = "{\"email\": \"" + Login1.UserName + "\", \"password\": \"" + Login1.Password + "\"}";
                    byte[] response = client.UploadData(Constants.AuthUrl, Constants.AuthMethod, Encoding.ASCII.GetBytes(authString));

                    ResponseToken = System.Text.Encoding.UTF8.GetString(response);
                    if (ResponseToken.Contains("\"id\":"))
                    {
                        var serializer = new JavaScriptSerializer();
                        LoginToken a = serializer.Deserialize<LoginToken>(ResponseToken);
                        Session.Add("ResponseToken", a.token);
                        Session.Add("UserName", Login1.UserName);
                        e.Authenticated = true;
                    }
                    else
                    {

                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
    }

    
    internal class LoginToken
    {
        public int id;
        public string token;
    }
    
}