﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCellDefinition.aspx.cs" Inherits="CDM_Manager.SearchCellDefinition" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <br />
        <br />
        <asp:Label ID="LabelCellId" runat="server" Text="Cell Id"></asp:Label>
&nbsp;
        <asp:TextBox ID="TextBoxCellId" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="Search" />
        <br />
        <br />
            <asp:GridView ID="SearchResultGridView" runat="server" AllowSorting="True" EmptyDataText="Cell definition not found. Please change the criteria" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFormatString="CellDefinition.aspx?celldefinitionId={0}&mode=edit" HeaderText="Edit/Delete Cell" DataNavigateUrlFields="Id" Text="Edit/Delete" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
    </form>
</body>
</html>
