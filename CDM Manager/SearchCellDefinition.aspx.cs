﻿using CDMManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CDM_Manager
{
    public partial class SearchCellDefinition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            SqlConnection sql;

            try
            {
                sql = new SqlConnection(Constants.CDManagerDB);
                DataTable table = new DataTable();
                string sqlGetDataSource = string.Format("SELECT Id, Title, LongTitle, CellDescription, ClusterCellId, Tags, LastUpdateUser, LastUpdateTime " +
                              "FROM[CDM_Dev].[Cdef].[CellDefinition] " +
                              "WHERE " +
                              "Id = {0} ",
                               TextBoxCellId.Text);
                SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                DataTable searchResultTable = new DataTable();
                adapter.Fill(searchResultTable);
                SearchResultGridView.DataSource = searchResultTable;
                SearchResultGridView.DataBind();
                

                if (searchResultTable.Rows.Count > 0)
                {
                    
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "ButtonSearch_Click");
                return;
            }
        }
    }
}