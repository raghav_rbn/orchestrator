﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CDMManager
{
    public class Utilities
    {
        public static void ResetAllControls(Control form)
        {
            foreach (Control control in form.Controls)
            {
                string x = control.ToString();

                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = null;
                }


                if (control is CheckBox)
                {
                    CheckBox checkBox = (CheckBox)control;
                    checkBox.Checked = false;
                }

 
                if (control is DropDownList)
                {
                    DropDownList dropDownList = (DropDownList)control;
                    dropDownList.ClearSelection();
                    
                }

                if (control is Panel)
                {
                    Panel panel = (Panel)control;
                    for (int i = 0; i < panel.Controls.Count; i++)
                    {
                        panel.Controls[i].Visible = false;
                        Control PanelControl = panel.Controls[i];


                        if (PanelControl is TextBox)
                        {
                            TextBox textBox = (TextBox)PanelControl;
                            textBox.Text = null;
                        }


                        if (PanelControl is CheckBox)
                        {
                            CheckBox checkBox = (CheckBox)PanelControl;
                            checkBox.Checked = false;
                        }


                        if (PanelControl is DropDownList)
                        {
                            DropDownList dropDownList = (DropDownList)PanelControl;
                            dropDownList.ClearSelection();
                            dropDownList.Items.Clear();
                        }
                    }
                }
            }
        }

        public static string FindAPIPostSizeConstant(String LayoutInput)
        {
            int j = Constants.CellSizeAPIMapping.GetLength(0);
            string returnVal = null;
            for (int i=0; i<j; i++)
            {
                if (String.Equals(Constants.CellSizeAPIMapping[i,0], LayoutInput)) {
                    returnVal = Constants.CellSizeAPIMapping[i, 1];
                    break;
                }
            }
            return returnVal;
        }

        public static Dictionary<int,string> LoadDictionaryFromTable(string idColumn, string valueColumn, string table)
        {
            SqlConnection sql = new SqlConnection(Constants.CDManagerDB);
            string sqlGetDataSource = "select " + idColumn + ", " + valueColumn + " from " + table;
            SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
            adapter.Fill(dataTable);
            return dataTable.AsEnumerable().ToDictionary<DataRow, int, string>(row => row.Field<int>(0), row => row.Field<string>(1));
        }

        protected static DateTime TranslateRelativePeriodStringToDate(String RelativePeriod)
        {
            DateTime today = DateTime.Now.Date;
            DateTime returnValue = DateTime.Now.Date;

            if (RelativePeriod.Equals("Current"))
                returnValue = today;
            else if (RelativePeriod.Contains("Day"))
            {
                int startOfDay = RelativePeriod.IndexOf("Day");
                int numberOfDays = Convert.ToInt16(RelativePeriod.Substring(0, startOfDay - 1));
                returnValue = today.AddDays(-numberOfDays);
            }
            else if (RelativePeriod.Contains("Week"))
            {
                int startOfWeek = RelativePeriod.IndexOf("Week");
                int numberOfWeeks = Convert.ToInt16(RelativePeriod.Substring(0, startOfWeek - 1));
                returnValue = today.AddDays(-numberOfWeeks * 7);
            }
            else if (RelativePeriod.Contains("Month"))
            {
                int startOfMonth = RelativePeriod.IndexOf("Month");
                int numberOfMonths = Convert.ToInt16(RelativePeriod.Substring(0, startOfMonth - 1));
                returnValue = today.AddMonths(-numberOfMonths);
            }
            else if (RelativePeriod.Contains("Year"))
            {
                int startOfYear = RelativePeriod.IndexOf("Year");
                int numberOfYears = Convert.ToInt16(RelativePeriod.Substring(0, startOfYear - 1));
                returnValue = today.AddYears(-numberOfYears);
            }


            return returnValue;
        }

        public static int RefreshNSDSingleCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CellSource, string CellDataType, string CellLocation, string CellType, string CellColor, string Tags)
        {
            SqlConnection sql = new SqlConnection(Constants.FlowDataDB);
            DataTable table = new DataTable();
            string SourceQuery = string.Format("select XrefID from Cdef.NSDXRef where NSDSourceID = {0} and NSDDataTypeID = {1} and NSDLocationID = {2}",
                CellSource,
                CellDataType,
                CellLocation);


            String sqlGetDataSource = string.Format("SELECT[FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                "WHERE " +
                "FlowDataSourceID = {0} " +
                "AND FlowDataDataTypeID = {1} " +
                "AND FlowDataLocationID = {2} " +
                "AND FlowDataCurrentRecord = 1 " +
                "AND FlowDataDateDeleted IS NULL " +
                "AND FlowDataDateReference in ( " +
                "select top 2 FlowDataDateReference " +
                "from [RBN_Staging].[Production].[tblFlowDataADC2] " +
                "where FlowDataSourceID = {0} " +
                "AND FlowDataDataTypeID = {1} " +
                "AND FlowDataLocationID = {2} " +
                "AND FlowDataCurrentRecord = 1 " +
                "AND FlowDataDateDeleted IS NULL " +
                "order by FlowDataDateReference desc)",
                CellSource,
                CellDataType,
                CellLocation);


            SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
            SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
            table.Clear();
            adapter.Fill(table);

            SingleNumericCell singleNumeric = new SingleNumericCell();
            singleNumeric.title = CellTitle;
            singleNumeric.layout = "numeric";
            singleNumeric.color = CellColor;
            if (singleNumeric.data != null && table.Rows.Count>0)
            {
                double temp = (double)table.Rows[1][2] - (double)table.Rows[0][2];
                singleNumeric.data.value = Convert.ToDouble(table.Rows[1][2]);
                //Suffix needed only for volume change and price change cell types
                if (CellType.Equals("Volume Change") || CellType.Equals("Price Change"))
                {
                    if (temp > 0)
                        singleNumeric.data.suffix = String.Concat("+", temp);
                    else
                        singleNumeric.data.suffix = String.Concat("", temp);
                }

                //trend needed only for price arrow cell types
                if (CellType.Equals("Price Arrow"))
                {
                    if (temp > 0)
                        singleNumeric.data.trend = 1;
                    else
                        singleNumeric.data.trend = 0;
                }

                //trend needed only for price change cell types
                if (CellType.Equals("Price Change"))
                {
                    if (temp > 0)
                        singleNumeric.data.suffixColor = "green";
                    else
                        singleNumeric.data.suffixColor = "red";
                }

            }

            if (Tags != null)
            {
                string[] tags = Tags.Trim().Split(',');
                singleNumeric.tags = tags;
            }

            string endPointResponse = null;
            String serializedResult = JsonConvert.SerializeObject(singleNumeric, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            if (ClusterCellId < 0)
                endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
            else
                endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
            SingleNumericCell response = JsonConvert.DeserializeObject<SingleNumericCell>(endPointResponse);
            int responseObjectid = (int)response.Id;
            UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
            return responseObjectid;
        }

        public static int RefreshNSDWideCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string CellSource,  string CellDataType, string CellLocation, string CellType, string CellColor, string Tags, string StrPeriod1, string StrPeriod2, string StrPeriod3,string Heading2, string Heading3)
        {
            SqlConnection sql;
            try
            {
                sql = new SqlConnection(Constants.FlowDataDB);
                DataTable table = new DataTable();

                WideNumericCell wideNumericCell = new WideNumericCell();
                wideNumericCell.title = CellTitle;
                wideNumericCell.layout = "numeric";
                wideNumericCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);
                wideNumericCell.color = CellColor;
                DateTime period1 = TranslateRelativePeriodStringToDate(StrPeriod1);
                DateTime period2 = TranslateRelativePeriodStringToDate(StrPeriod2);
                DateTime period3 = TranslateRelativePeriodStringToDate(StrPeriod3);
                //Model the string as below
                //wideNumericCell.data.table = new string[,] { { "8/15", "Week", "Year" }, { "2.149", "-.001", "567" } };
                //wideNumericCell.data.table = new string[,] { { period1.ToString("MM/dd"), TextBoxHeading2.Text, TextBoxHeading3.Text }, { "2.149", "-.001", "567" } };

                wideNumericCell.data.table = new string[2, 3];
                wideNumericCell.data.table[0, 0] = period1.ToString("MM/dd");
                wideNumericCell.data.table[0, 1] = Heading2;
                wideNumericCell.data.table[0, 2] = Heading3;


                String sqlGetDataSource = string.Format("SELECT[FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                    "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                    "WHERE " +
                    "FlowDataSourceID = {0} " +
                    "AND FlowDataDataTypeID = {1} " +
                    "AND FlowDataLocationID = {2} " +
                    "AND FlowDataCurrentRecord = 1 " +
                    "AND FlowDataDateDeleted IS NULL " +
                    "AND FlowDataDateReference > '{3}' " +
                    "order by FlowDataDateReference desc",
                    CellSource,
                    CellDataType,
                    CellLocation,
                    period3.ToString("yyyy-MM-dd"));

                SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                table.Clear();
                adapter.Fill(table);

                //This is the current value
                wideNumericCell.data.table[1, 0] = Convert.ToString(table.Rows[0][2]);
                DateTime latestDateTime = Convert.ToDateTime(table.Rows[0][1]);
                //Calculation for difference of current value and NSD record from one year ago
                double temp = (double)table.Rows[0][2] - (double)table.Rows[table.Rows.Count - 1][2];
                if (temp > 0)
                    wideNumericCell.data.table[1, 2] = String.Concat("+", temp);
                else
                    wideNumericCell.data.table[1, 2] = String.Concat("", temp);


                sqlGetDataSource = string.Format("SELECT top 1 [FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                               "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                               "WHERE " +
                               "FlowDataSourceID = {0} " +
                               "AND FlowDataDataTypeID = {1} " +
                               "AND FlowDataLocationID = {2} " +
                               "AND FlowDataCurrentRecord = 1 " +
                               "AND FlowDataDateDeleted IS NULL " +
                               "AND FlowDataDateReference < '{3}' " +
                               "AND FlowDataDateReference <> '{4}' " +
                               "order by FlowDataDateReference desc",
                               CellSource,
                               CellDataType,
                               CellLocation,
                               period2.ToString("yyyy-MM-dd"),
                               latestDateTime.ToString("yyyy-MM-dd"));
                selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                adapter = new SqlDataAdapter(selectDataSource);
                DataTable Period2table = new DataTable();
                adapter.Fill(Period2table);
                temp = (double)table.Rows[0][2] - (double)Period2table.Rows[0][2];
                if (temp > 0)
                    wideNumericCell.data.table[1, 1] = String.Concat("+", temp);
                else
                    wideNumericCell.data.table[1, 1] = String.Concat("", temp);

                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    wideNumericCell.tags = tags;
                }

                String serializedResult = JsonConvert.SerializeObject(wideNumericCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = null;
                if (ClusterCellId < 0)
                    endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                else
                    endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
                WideNumericCell response = JsonConvert.DeserializeObject<WideNumericCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                sql.Close();
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshNSDWideCellDefinition");
                return -1;
            }
        }

        public static int RefreshNSDTallCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string[] CellSource, string[] CellDataType, string[] CellLocation, string CellType, string CellColor, string Tags, string heading, string period, string[] xAxisLabel)
        {
            SqlConnection sql;
            
            try
            {
                sql = new SqlConnection(Constants.FlowDataDB);
                DataTable table = new DataTable();
                TallNumericCell tallNumericCell = new TallNumericCell();
                tallNumericCell.title = CellTitle;
                tallNumericCell.layout = "numeric";
                tallNumericCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);
                tallNumericCell.color = CellColor;

                int TallSourceCount = CellSource.Length;
                tallNumericCell.data.table = new string[TallSourceCount + 1, 2];
                tallNumericCell.data.table[0, 0] = heading;

                DateTime NSDTallPeriod = TranslateRelativePeriodStringToDate(period);
                tallNumericCell.data.table[0, 1] = NSDTallPeriod.ToString("MM/dd");

                for (int i = 1; i <= TallSourceCount; i++)
                {
                    if (!CellSource[i-1].Equals("0")) 
                        PopulateTallElements(sql, tallNumericCell, i, CellSource[i-1], CellDataType[i-1], CellLocation[i-1], NSDTallPeriod, xAxisLabel[i-1]);
                }

                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    tallNumericCell.tags = tags;
                }

                String serializedResult = JsonConvert.SerializeObject(tallNumericCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = null;
                if (ClusterCellId < 0)
                    endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                else
                    endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
                TallNumericCell response = JsonConvert.DeserializeObject<TallNumericCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                sql.Close();
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshNSDTallCellDefinition");
                return -1;
            }
        }

        public static int RefreshNSDQuadNonGraphViewCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string[] CellSource, string[] CellDataType, string[] CellLocation, string CellType, string CellColor, string Tags, string[] heading, string[] period, string[] xAxisLabel)
        {
            SqlConnection sql;
            
            try
            {
                sql = new SqlConnection(Constants.FlowDataDB);
                DataTable table = new DataTable();
                QuadNumericCell quadNumericCell = new QuadNumericCell();
                quadNumericCell.title = CellTitle;
                quadNumericCell.layout = "numeric";
                quadNumericCell.color = CellColor;
                quadNumericCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);

                int QuadSourceCount = CellSource.Length;
                quadNumericCell.data.table = new string[QuadSourceCount + 1, 4];
 
                quadNumericCell.data.table[0, 0] = heading[0];
                DateTime NSDQuadPeriod1 = TranslateRelativePeriodStringToDate(period[0]);
                quadNumericCell.data.table[0, 1] = NSDQuadPeriod1.ToString("MM/dd");
                quadNumericCell.data.table[0, 2] = heading[1];
                quadNumericCell.data.table[0, 3] = heading[2];
                DateTime NSDQuadPeriod2 = TranslateRelativePeriodStringToDate(period[1]);
                DateTime NSDQuadPeriod3 = TranslateRelativePeriodStringToDate(period[2]);

                for (int i = 1; i <= QuadSourceCount; i++)
                {
                    PopulateNonGraphQuadElements(sql, quadNumericCell, i, CellSource[i-1], CellDataType[i-1], CellLocation[i-1], NSDQuadPeriod1, NSDQuadPeriod2, NSDQuadPeriod3, xAxisLabel[i - 1]);
                }

                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    quadNumericCell.tags = tags;
                }
                String serializedResult = JsonConvert.SerializeObject(quadNumericCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = null;
                if (ClusterCellId < 0)
                    endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                else
                    endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
                QuadNumericCell response = JsonConvert.DeserializeObject<QuadNumericCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                sql.Close();
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshNSDQuadNonGraphViewCellDefinition");
                return -1;
            }
        }

        public static int RefreshWideGraphCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string CellSource, string CellDataType, string CellLocation, string CellType, string CellColor, string Tags, string NumOfEvents, string CDLineColor, string Legend, string Heading3)
        {
            SqlConnection sql;

            try
            {
                sql = new SqlConnection(Constants.FlowDataDB);
                WideNumericGraphCell wideNumericGraphCell = new WideNumericGraphCell();
                wideNumericGraphCell.title = CellTitle;
                wideNumericGraphCell.layout = "graphic";
                wideNumericGraphCell.color = CellColor;
                wideNumericGraphCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);
                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    wideNumericGraphCell.tags = tags;
                }
                DataTable table = new DataTable();

                String sqlGetDataSource = string.Format("SELECT top {0} " +
                               "[FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                               "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                               "WHERE " +
                               "FlowDataSourceID = {1} " +
                               "AND FlowDataDataTypeID = {2} " +
                               "AND FlowDataLocationID = {3} " +
                               "AND FlowDataCurrentRecord = 1 " +
                               "AND FlowDataDateDeleted IS NULL " +
                               "order by FlowDataDateReference desc",
                               Convert.ToInt32(NumOfEvents) + 1,
                               CellSource,
                               CellDataType,
                               CellLocation);
                SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);



                //Below prototype is used for posting
                //wideNumericGraphCell.data.captions = new string[,] { { "2/22", "2.149" }, { "20", "Day" } };
                //wideNumericGraphCell.data.labels = new string[] { "8/26/2017", "8/25/2017", "8/24/2017", "8/23/2017", "8/22/2017" };
                //wideNumericGraphCell.data.chartData[0].label = "Gas Prices";
                //wideNumericGraphCell.data.chartData[0].data = new double[] {1.23, 1.34, 1.56, 1.88, 1.95 };
                //wideNumericGraphCell.data.chartOptions.linecolor = new string[] {"blue"};

                wideNumericGraphCell.data.chartData[0].label = Legend;
                wideNumericGraphCell.data.chartOptions.lineColor = new string[1];
                wideNumericGraphCell.data.chartOptions.lineColor[0] = CDLineColor;
                wideNumericGraphCell.data.captions = new string[2, 2];
                wideNumericGraphCell.data.captions[0, 0] = ((DateTime)dataTable.Rows[0][1]).ToString("MM/dd");
                wideNumericGraphCell.data.captions[0, 1] = Convert.ToString(dataTable.Rows[0][2]);
                wideNumericGraphCell.data.captions[1, 0] = NumOfEvents;
                wideNumericGraphCell.data.captions[1, 1] = Heading3;
                wideNumericGraphCell.data.labels = new string[dataTable.Rows.Count - 1];
                wideNumericGraphCell.data.chartData[0].data = new double[dataTable.Rows.Count - 1];

                for (int i = 0; i < dataTable.Rows.Count - 1; i++)
                {
                    wideNumericGraphCell.data.labels[i] = ((DateTime)dataTable.Rows[i + 1][1]).ToString("MM/dd/yyyy");
                    wideNumericGraphCell.data.chartData[0].data[i] = Convert.ToDouble(dataTable.Rows[i + 1][2]);
                }

                String serializedResult = JsonConvert.SerializeObject(wideNumericGraphCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = null;
                if (ClusterCellId < 0)
                    endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                else
                    endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
                WideNumericGraphCell response = JsonConvert.DeserializeObject<WideNumericGraphCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                sql.Close();
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshWideGraphCellDefinition");
                return -1;
            }
        }

        public static int RefreshTallGraphCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string CellSource, string CellDataType, string CellLocation, string CellType, string CellColor, string Tags, string NumOfEvents, string CDLineColor, string Legend, string YAxis, string Heading)
        {
            SqlConnection sql;

            try
            {
                sql = new SqlConnection(Constants.FlowDataDB);
                TallNumericGraphCell tallNumericGraphCell = new TallNumericGraphCell();
                tallNumericGraphCell.title = CellTitle;
                tallNumericGraphCell.layout = "graphic";
                tallNumericGraphCell.color = CellColor;
                tallNumericGraphCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);
                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    tallNumericGraphCell.tags = tags;
                }
                DataTable table = new DataTable();

                String sqlGetDataSource = string.Format("SELECT top {0} " +
                               "[FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                               "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                               "WHERE " +
                               "FlowDataSourceID = {1} " +
                               "AND FlowDataDataTypeID = {2} " +
                               "AND FlowDataLocationID = {3} " +
                               "AND FlowDataCurrentRecord = 1 " +
                               "AND FlowDataDateDeleted IS NULL " +
                               "order by FlowDataDateReference desc",
                               Convert.ToInt32(NumOfEvents) + 1,
                               CellSource,
                               CellDataType,
                               CellLocation);
                SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                //Below prototype is used for posting
                //tallNumericGraphCell.data.caption = "20 Day";
                //tallNumericGraphCell.data.labels = new string[] { "8/26/2017", "8/25/2017", "8/24/2017", "8/23/2017", "8/22/2017" };
                //tallNumericGraphCell.data.chartData[0].label = "Gas Prices";
                //tallNumericGraphCell.data.chartData[0].data = new double[] {1.23, 1.34, 1.56, 1.88, 1.95 };
                //tallNumericGraphCell.data.chartOptions.linecolor = new string[] {"blue"};
                //tallNumericGraphCell.data.chartOptions.axisType = new string[] {"Percent"};

                tallNumericGraphCell.data.chartData[0].label = Legend;
                tallNumericGraphCell.data.chartOptions.lineColor = new string[1];
                tallNumericGraphCell.data.chartOptions.lineColor[0] = CDLineColor;
                tallNumericGraphCell.data.chartOptions.axisType = new string[1];
                tallNumericGraphCell.data.chartOptions.axisType[0] = YAxis;
                tallNumericGraphCell.data.caption = Heading;
                tallNumericGraphCell.data.labels = new string[dataTable.Rows.Count - 1];
                tallNumericGraphCell.data.chartData[0].data = new double[dataTable.Rows.Count - 1];

                for (int i = 0; i < dataTable.Rows.Count - 1; i++)
                {
                    tallNumericGraphCell.data.labels[i] = ((DateTime)dataTable.Rows[i + 1][1]).ToString("MM/dd/yyyy");
                    tallNumericGraphCell.data.chartData[0].data[i] = Convert.ToDouble(dataTable.Rows[i + 1][2]);
                }

                String serializedResult = JsonConvert.SerializeObject(tallNumericGraphCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = null;
                if (ClusterCellId < 0)
                    endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                else
                    endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
                TallNumericGraphCell response = JsonConvert.DeserializeObject<TallNumericGraphCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                Utilities.UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                sql.Close();
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshTallGraphCellDefinition");
                return -1;
            }
        }

        public static int RefreshNSDQuadGraphViewCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string[] CellSource, string[] CellDataType, string[] CellLocation, string CellType, string CellColor, string Tags, string[] xAxisLabel, string[] LineColor, string GraphPeriod, string GraphNumOfEvents)
        {
            SqlConnection sql;
            
            try
            {
                sql = new SqlConnection(Constants.FlowDataDB);
                DataTable table = new DataTable();
                QuadNumericGraphCell quadNumericGraphCell = new QuadNumericGraphCell();
                quadNumericGraphCell.title = CellTitle;
                quadNumericGraphCell.layout = "graphic";
                quadNumericGraphCell.color = CellColor;
                quadNumericGraphCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);

                //Below prototype is used for posting
                /*
                quadNumericGraphCell.data.labels = new string[] { "8/26/2017", "8/25/2017", "8/24/2017", "8/23/2017", "8/22/2017" };
                quadNumericGraphCell.data.chartData = new ChartData[3] { new ChartData(), new ChartData(), new ChartData() };
                quadNumericGraphCell.data.chartData[0].label = "PADD1";
                quadNumericGraphCell.data.chartData[0].data = new double[] {3.79, 7 };
                quadNumericGraphCell.data.chartData[1].label = "PADD2";
                quadNumericGraphCell.data.chartData[1].data = new double[] {27.553, 27.183 };
                quadNumericGraphCell.data.chartData[2].label = "PADD3";
                quadNumericGraphCell.data.chartData[2].data = new double[] { 17.553, 17.183 };
                quadNumericGraphCell.data.chartOptions.lineColor = new string[] {"blue", "red", "green"};
                */


                int QuadSourceCount = CellSource.Length;

                switch (QuadSourceCount)
                {
                    case 1:
                        quadNumericGraphCell.data.chartData = new ChartData[1] { new ChartData() };
                        break;
                    case 2:
                        quadNumericGraphCell.data.chartData = new ChartData[2] { new ChartData(), new ChartData() };
                        break;
                    case 3:
                        quadNumericGraphCell.data.chartData = new ChartData[3] { new ChartData(), new ChartData(), new ChartData() };
                        break;
                    case 4:
                        quadNumericGraphCell.data.chartData = new ChartData[4] { new ChartData(), new ChartData(), new ChartData(), new ChartData() };
                        break;
                }


                quadNumericGraphCell.data.chartOptions.lineColor = new string[QuadSourceCount];
                DateTime NSDQuadPeriod = TranslateRelativePeriodStringToDate(GraphPeriod);
                int NumOfEvents = Convert.ToInt32(GraphNumOfEvents);
                quadNumericGraphCell.data.labels = new string[NumOfEvents];

                for (int i = 1; i <= QuadSourceCount; i++)
                {
                    PopulateQuadGraphElements(sql, quadNumericGraphCell, i, CellSource[i - 1], CellDataType[i - 1], CellLocation[i - 1], NSDQuadPeriod, NumOfEvents, xAxisLabel[i-1], LineColor[i-1]);
                }

                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    quadNumericGraphCell.tags = tags;
                }

                String serializedResult = JsonConvert.SerializeObject(quadNumericGraphCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = null;
                if (ClusterCellId < 0)
                    endPointResponse = PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                else
                    endPointResponse = PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, false);
                QuadNumericGraphCell response = JsonConvert.DeserializeObject<QuadNumericGraphCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                sql.Close();
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshNSDQuadGraphViewCellDefinition");
                return -1;
            }
        }

        public static int RefreshOtherCellDefinition(String ResponseToken, int CellDefinitionId, int ClusterCellId, string CellTitle, string CDLayout, string CDInput, string CDColor, string CDCellType, string Tags, string ImagePath, string ImageOverlay, string OverlayColorValue, string OverlayColorItem, string OverlayPositionValue, string OverlayPositionItem)
        {
            try
            {
                OtherCell OtherCell = new OtherCell();
                OtherCell.title = CellTitle;
                OtherCell.size = Utilities.FindAPIPostSizeConstant(CDLayout);
                OtherCell.color = CDColor;
                if (CDInput.Equals("Other") &&
                    (CDCellType.Equals("Image")) || (CDCellType.Equals("Image W Overlay")))
                {
                    OtherCell.layout = "image";
                    if (OtherCell.data != null)
                    {
                        OtherCell.data.imgSrc = ImagePath;
                        if (CDCellType.ToString().Equals("Image W Overlay"))
                        {
                            if (ImageOverlay != null)
                            {
                                OtherCell.data.captionText = ImageOverlay;
                            }
                            if (OverlayColorValue != null)
                            {
                                OtherCell.data.captionOptions.color = OverlayColorItem;
                            }
                            if (OverlayPositionValue != null)
                            {
                                OtherCell.data.captionOptions.location = OverlayPositionItem;
                            }
                        }
                    }

                }

                if (Tags != null)
                {
                    string[] tags = Tags.Trim().Split(',');
                    OtherCell.tags = tags;
                }

                String serializedResult = JsonConvert.SerializeObject(OtherCell, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = Utilities.PostToEndPoint(ResponseToken, Constants.CellEndPoint, serializedResult);
                OtherCell response = JsonConvert.DeserializeObject<OtherCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                Utilities.UpdateClusterCellIDinCellDefinition(responseObjectid, CellDefinitionId);
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RefreshOtherCellDefinition");
                return -1;
            }
        }

        public static int ModalCellPost(String ResponseToken, int ClusterCellId, string modalLayout, string modalLongTitle, string modalImageNewsPath, string[] ModalCellSource, string[] ModalDataType, string[] ModalLocation, string[] ModalPeriod, string[] ModalNumOfEvents, string[] ModalLineColor, string[] ModalLabel)
        {
            try
            {

                ModalUpdate modalUpdate = new ModalUpdate();
                modalUpdate.id = Int32.Parse(ClusterCellId.ToString());
                modalUpdate.longTitle = modalLongTitle;
                modalUpdate.modalLayout = modalLayout;
                if (modalLayout.Equals("webView"))
                {
                    modalUpdate.modalDetail = new ModalDetail();
                    modalUpdate.modalDetail.url = modalImageNewsPath;
                }
                else if (modalLayout.Equals("Graph"))
                {
                    modalUpdate.modalLayout = "lineChart";
                    modalUpdate.modalDetail.cellId = ClusterCellId;
                    modalUpdate.modalDetail.chartData = new ModalOuterChartData();
                    modalUpdate.modalDetail.chartData.chartData = new ChartData[] { new ChartData() };
                    modalUpdate.modalDetail.chartData.chartOptions = new ChartOptions();
                    int modalCellSourceCount = 0;

                    for (int arrayLen=1; arrayLen<= ModalCellSource.Length; arrayLen++)
                    {
                        if (!ModalCellSource[arrayLen-1].Equals("0"))
                            modalCellSourceCount = arrayLen;
                    }

                    switch (modalCellSourceCount)
                    {
                        case 1:
                            modalUpdate.modalDetail.chartData.chartData = new ChartData[1] { new ChartData() };
                            break;
                        case 2:
                            modalUpdate.modalDetail.chartData.chartData = new ChartData[2] { new ChartData(), new ChartData() };
                            break;
                        case 3:
                            modalUpdate.modalDetail.chartData.chartData = new ChartData[3] { new ChartData(), new ChartData(), new ChartData() };
                            break;
                        case 4:
                            modalUpdate.modalDetail.chartData.chartData = new ChartData[4] { new ChartData(), new ChartData(), new ChartData(), new ChartData() };
                            break;
                    }

                    modalUpdate.modalDetail.chartData.chartOptions.lineColor = new string[modalCellSourceCount];
                    using (SqlConnection sql = new SqlConnection(Constants.FlowDataDB))
                    {
                        for (int i = 1; i <= modalCellSourceCount; i++)
                        {
                            PopulateModalGraphElements(sql, modalUpdate.modalDetail.chartData, i, ModalCellSource[i-1], ModalDataType[i - 1], ModalLocation[i - 1], ModalPeriod[i - 1], ModalNumOfEvents[i - 1], ModalLineColor[i - 1], ModalLabel[i - 1]);
                        }
                    }

                    //Below prototype is used for posting
                    /*
                    modalUpdate.modalDetail.chartData.label = new string[] { "8/26/2017", "8/25/2017", "8/24/2017", "8/23/2017", "8/22/2017" };
                    modalUpdate.modalDetail.chartData.chartData = new ChartData[3] { new ChartData(), new ChartData(), new ChartData() };
                    modalUpdate.modalDetail.chartData.chartData[0].label = "PADD1";
                    modalUpdate.modalDetail.chartData.chartData[0].data = new double[] { 3.79, 7 };
                    modalUpdate.modalDetail.chartData.chartData[1].label = "PADD2";
                    modalUpdate.modalDetail.chartData.chartData[1].data = new double[] { 27.553, 27.183 };
                    modalUpdate.modalDetail.chartData.chartData[2].label = "PADD3";
                    modalUpdate.modalDetail.chartData.chartData[2].data = new double[] { 17.553, 17.183 };
                    modalUpdate.modalDetail.chartData.chartOptions.lineColor = new string[] { "blue", "red", "green" };
                    */
                }

                String serializedResult = JsonConvert.SerializeObject(modalUpdate, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string endPointResponse = Utilities.PutToEndPoint(ResponseToken, Constants.CellEndPoint, ClusterCellId.ToString(), serializedResult, true);
                OtherCell response = JsonConvert.DeserializeObject<OtherCell>(endPointResponse);
                int responseObjectid = (int)response.Id;
                return responseObjectid;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "ModalNewslinkPost");
                return -1;
            }
        }

        protected static void PopulateTallElements(SqlConnection sql, TallNumericCell tallNumericCell, int i, string NSDTallSource, string NSDTallType, string NSDTallLocation, DateTime NSDTallPeriod, string RowLabel)
        {
            try
            {
                DataTable table = new DataTable();
                String sqlGetDataSource;
                SqlCommand selectDataSource;
                SqlDataAdapter adapter;

                sqlGetDataSource = string.Format("SELECT[FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                    "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                    "WHERE " +
                    "FlowDataSourceID = {0} " +
                    "AND FlowDataDataTypeID = {1} " +
                    "AND FlowDataLocationID = {2} " +
                    "AND FlowDataCurrentRecord = 1 " +
                    "AND FlowDataDateDeleted IS NULL " +
                    "AND FlowDataDateReference <= '{3}' " +
                    "order by FlowDataDateReference desc",
                    NSDTallSource,
                    NSDTallType,
                    NSDTallLocation,
                    NSDTallPeriod.ToString("yyyy-MM-dd"));

                selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                adapter = new SqlDataAdapter(selectDataSource);
                table.Clear();
                adapter.Fill(table);
                tallNumericCell.data.table[i, 0] = RowLabel;
                tallNumericCell.data.table[i, 1] = Convert.ToString(table.Rows[0][2]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "populateTallElements");
            }
        }

        protected static void PopulateNonGraphQuadElements(SqlConnection sql, QuadNumericCell quadNumericCell, int i, string NSDQuadSource, string NSDQuadType, string NSDQuadLocation, DateTime Period1, DateTime Period2, DateTime Period3, string RowLabel)
        {
            try
            {
                DataTable table = new DataTable();
                String sqlGetDataSource;
                SqlCommand selectDataSource;
                SqlDataAdapter adapter;

                sqlGetDataSource = string.Format("SELECT[FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                    "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                    "WHERE " +
                    "FlowDataSourceID = {0} " +
                    "AND FlowDataDataTypeID = {1} " +
                    "AND FlowDataLocationID = {2} " +
                    "AND FlowDataCurrentRecord = 1 " +
                    "AND FlowDataDateDeleted IS NULL " +
                    "AND FlowDataDateReference > '{3}' " +
                    "order by FlowDataDateReference desc",
                    NSDQuadSource,
                    NSDQuadType,
                    NSDQuadLocation,
                    Period3.ToString("yyyy-MM-dd"));

                selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                adapter = new SqlDataAdapter(selectDataSource);
                table.Clear();
                adapter.Fill(table);

                //This is the current value
                if (table.Rows.Count > 0)
                {
                    quadNumericCell.data.table[i, 1] = Convert.ToString(table.Rows[0][2]);
                    DateTime latestDateTime = Convert.ToDateTime(table.Rows[0][1]);
                    //Calculation for difference of current value and NSD record from one year ago
                    double temp = (double)table.Rows[0][2] - (double)table.Rows[table.Rows.Count - 1][2];
                    if (temp > 0)
                        quadNumericCell.data.table[i, 3] = String.Concat("+", temp);
                    else
                        quadNumericCell.data.table[i, 3] = String.Concat("", temp);


                    sqlGetDataSource = string.Format("SELECT top 1 [FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                                   "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                                   "WHERE " +
                                   "FlowDataSourceID = {0} " +
                                   "AND FlowDataDataTypeID = {1} " +
                                   "AND FlowDataLocationID = {2} " +
                                   "AND FlowDataCurrentRecord = 1 " +
                                   "AND FlowDataDateDeleted IS NULL " +
                                   "AND FlowDataDateReference < '{3}' " +
                                   "AND FlowDataDateReference <> '{4}' " +
                                   "order by FlowDataDateReference desc",
                                   NSDQuadSource,
                                   NSDQuadType,
                                   NSDQuadLocation,
                                   Period2.ToString("yyyy-MM-dd"),
                                   latestDateTime.ToString("yyyy-MM-dd"));
                    selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                    adapter = new SqlDataAdapter(selectDataSource);
                    DataTable Period2table = new DataTable();
                    adapter.Fill(Period2table);
                    temp = (double)table.Rows[0][2] - (double)Period2table.Rows[0][2];
                    if (temp > 0)
                        quadNumericCell.data.table[i, 2] = String.Concat("+", temp);
                    else
                        quadNumericCell.data.table[i, 2] = String.Concat("", temp);

                    quadNumericCell.data.table[i, 0] = RowLabel;
                }
                else
                {
                    ExceptionUtility.LogMessage("SQL statement " + sqlGetDataSource + " does not have rows", "PopulateNonGraphQuadElements");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "PopulateNonGraphQuadElements");
            }
        }

        protected static void PopulateQuadGraphElements(SqlConnection sql, QuadNumericGraphCell quadNumericGraphCell, int elementCounter, string NSDQuadGraphSource, string NSDQuadGraphType, string NSDQuadGraphLocation, DateTime NSDQuadGraphPeriod, int QuadGraphNumOfEvents, string QuadGraphXAxisLabel, string LineColor)
        {
            try
            {
                DataTable table = new DataTable();
                String sqlGetDataSource;
                SqlCommand selectDataSource;
                SqlDataAdapter adapter;

                //Below prototype is used for posting
                /*
                quadNumericGraphCell.data.labels = new string[] { "8/26/2017", "8/25/2017", "8/24/2017", "8/23/2017", "8/22/2017" };
                quadNumericGraphCell.data.chartData = new ChartData[3] { new ChartData(), new ChartData(), new ChartData() };
                quadNumericGraphCell.data.chartData[0].label = "PADD1";
                quadNumericGraphCell.data.chartData[0].data = new double[] { 3.79, 7 };
                quadNumericGraphCell.data.chartData[1].label = "PADD2";
                quadNumericGraphCell.data.chartData[1].data = new double[] { 27.553, 27.183 };
                quadNumericGraphCell.data.chartData[2].label = "PADD3";
                quadNumericGraphCell.data.chartData[2].data = new double[] { 17.553, 17.183 };
                quadNumericGraphCell.data.chartOptions.lineColor = new string[] { "blue", "red", "green" };
                */

                quadNumericGraphCell.data.chartOptions.lineColor[elementCounter - 1] = LineColor;

                sqlGetDataSource = string.Format("SELECT top {0} [FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                    "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                    "WHERE " +
                    "FlowDataSourceID = {1} " +
                    "AND FlowDataDataTypeID = {2} " +
                    "AND FlowDataLocationID = {3} " +
                    "AND FlowDataCurrentRecord = 1 " +
                    "AND FlowDataDateDeleted IS NULL " +
                    "AND FlowDataDateReference < '{4}' " +
                    "order by FlowDataDateReference desc",
                    QuadGraphNumOfEvents,
                    NSDQuadGraphSource,
                    NSDQuadGraphType,
                    NSDQuadGraphLocation,
                    NSDQuadGraphPeriod.ToString("yyyy-MM-dd"));

                selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                adapter = new SqlDataAdapter(selectDataSource);
                table.Clear();
                adapter.Fill(table);
                quadNumericGraphCell.data.chartData[elementCounter - 1].data = new double[table.Rows.Count];
                quadNumericGraphCell.data.chartData[elementCounter - 1].label = QuadGraphXAxisLabel;

                for (int j = 0; j < table.Rows.Count; j++)
                {
                    if (elementCounter == 1)
                        quadNumericGraphCell.data.labels[j] = ((DateTime)table.Rows[j][1]).ToString("MM/dd/yyyy");
                    quadNumericGraphCell.data.chartData[elementCounter - 1].data[j] = Convert.ToDouble(table.Rows[j][2]);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "populateQuadGraphElements");
            }
        }

        protected static void PopulateModalGraphElements(SqlConnection sql, ModalOuterChartData ChartData, int elementCounter, string ModalCellSource, string ModalDataType, string ModalLocation, string ModalPeriod, string ModalNumOfEvents, string ModalLineColor, string ModalLabel)
        {
            try
            {
                //Below prototype is used for posting
                /*
                modalUpdate.modalDetail.chartData.label = new string[] { "8/26/2017", "8/25/2017", "8/24/2017", "8/23/2017", "8/22/2017" };
                modalUpdate.modalDetail.chartData.chartData = new ChartData[3] { new ChartData(), new ChartData(), new ChartData() };
                modalUpdate.modalDetail.chartData.chartData[0].label = "PADD1";
                modalUpdate.modalDetail.chartData.chartData[0].data = new double[] { 3.79, 7 };
                modalUpdate.modalDetail.chartData.chartData[1].label = "PADD2";
                modalUpdate.modalDetail.chartData.chartData[1].data = new double[] { 27.553, 27.183 };
                modalUpdate.modalDetail.chartData.chartData[2].label = "PADD3";
                modalUpdate.modalDetail.chartData.chartData[2].data = new double[] { 17.553, 17.183 };
                modalUpdate.modalDetail.chartData.chartOptions.lineColor = new string[] { "blue", "red", "green" };
                */

                ChartData.chartOptions.lineColor[elementCounter - 1] = ModalLineColor;

                String sqlGetDataSource = string.Format("SELECT top {0} [FlowDataID],[FlowDataDateReference],[FlowDataVolume] " +
                    "FROM[RBN_Staging].[Production].[tblFlowDataADC2] " +
                    "WHERE " +
                    "FlowDataSourceID = {1} " +
                    "AND FlowDataDataTypeID = {2} " +
                    "AND FlowDataLocationID = {3} " +
                    "AND FlowDataCurrentRecord = 1 " +
                    "AND FlowDataDateDeleted IS NULL " +
                    "AND FlowDataDateReference < '{4}' " +
                    "order by FlowDataDateReference desc",
                    ModalNumOfEvents,
                    ModalCellSource,
                    ModalDataType,
                    ModalLocation,
                    TranslateRelativePeriodStringToDate(ModalPeriod).ToString("yyyy-MM-dd"));

                SqlCommand selectDataSource = new SqlCommand(sqlGetDataSource, sql);
                SqlDataAdapter adapter = new SqlDataAdapter(selectDataSource);
                DataTable table = new DataTable();
                adapter.Fill(table);
                ChartData.chartData[elementCounter - 1].data = new double[table.Rows.Count];
                ChartData.chartData[elementCounter - 1].label = ModalLabel;
                if (elementCounter == 1)
                    ChartData.label = new string[table.Rows.Count];

                for (int j = 0; j < table.Rows.Count; j++)
                {
                    ChartData.label[j] = ((DateTime)table.Rows[j][1]).ToString("MM/dd/yyyy");
                    ChartData.chartData[elementCounter - 1].data[j] = Convert.ToDouble(table.Rows[j][2]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "PopulateModalGraphElements");
            }
        }

        public static string PostToEndPoint(string ResponseToken, string endPoint, string serializedObject)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    string AuthHeader = "Bearer " + ResponseToken;
                    client.Headers["Authorization"] = AuthHeader;
                    client.Headers["Content-Type"] = Constants.AuthContentType;
                    byte[] response = client.UploadData(endPoint, Constants.AuthMethod, Encoding.ASCII.GetBytes(serializedObject));

                    string EndPointResponse = System.Text.Encoding.UTF8.GetString(response);
                    return EndPointResponse;
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "postToEndPoint");
                return null;
            }
        }

        public static string PutToEndPoint(string ResponseToken, string endPoint, string clusterCellId, string serializedObject, Boolean isModal)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    string AuthHeader = "Bearer " + ResponseToken;
                    client.Headers["Authorization"] = AuthHeader;
                    client.Headers["Content-Type"] = Constants.AuthContentType;
                    string target;
                    if (isModal)
                        target = String.Concat(endPoint, "/", clusterCellId, "/detail");
                    else
                        target = String.Concat(endPoint, "/", clusterCellId);
                    byte[] response = client.UploadData(target, "PUT", Encoding.ASCII.GetBytes(serializedObject));

                    string EndPointResponse = System.Text.Encoding.UTF8.GetString(response);
                    return EndPointResponse;
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "putToEndPoint");
                return null;
            }
        }

        public static bool UpdateClusterCellIDinCellDefinition(int clusterCellId, int cellDefId)
        {
            bool status = false;
            SqlCommand CellIdUpdateSql = null;
            int numRowsUpdated = -1;
            DateTime dateNow = DateTime.Now;
            using (SqlConnection conn = new SqlConnection(Constants.CDManagerDB))
            {
                conn.Open();
                CellIdUpdateSql = new SqlCommand(
                    "update Cdef.CellDefinition " +
                    " set ClusterCellId = " + clusterCellId + "," +
                    " LastUpdateTime = '" + dateNow +  "'" +
                    " where Id = " + cellDefId,
                  conn);
                numRowsUpdated = CellIdUpdateSql.ExecuteNonQuery();
                conn.Close();
            }
            if (numRowsUpdated > 0) status = true;
            return status;
        }

        public static DataTable RunSql(SqlConnection sql, String SQLString)
        {
            DataTable table = new DataTable();

            try
            {
                SqlCommand selectDataSource;
                SqlDataAdapter adapter;
                
                selectDataSource = new SqlCommand(SQLString, sql);
                adapter = new SqlDataAdapter(selectDataSource);
                table.Clear();
                adapter.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ExceptionUtility.LogException(ex, "RunSql");
                return table;
            }
        }
    }
}